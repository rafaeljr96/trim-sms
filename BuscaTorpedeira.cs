﻿using EnviarSMS;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO.Ports;
using System.Management;
using System.Windows.Forms;

namespace Torpedos
{
    public partial class BuscaTorpedeira : Form
    {
        List<string> Portas = new List<string>();
        private static string Numero;
        private static string Mensagem;
        private SmsClass sms= new SmsClass();
        private SerialPort sp;
        public BuscaTorpedeira()
        {
            InitializeComponent();
            BuscarTordeira();
        }

        private void BuscaTorpedeira_Load(object sender, EventArgs e)
        {
            txtTelefone.Text = "18996106757";
        }
        void sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var aux = "";
            if (e.EventType != SerialData.Chars)
            {
                aux = "IGNORED: " + e.EventType.ToString();
                return;
            }
            try
            {
                int nBytesToRead = sp.BytesToRead;
                char[] receivedData = new char[nBytesToRead];
                sp.Read(receivedData, 0, nBytesToRead);
                aux = "RECEIVED: " + new string(receivedData);
            }
            catch (Exception ex)
            {
                aux = "ERROR: " + ex.Message;
            }
        }
        private void BuscarTordeira()
        {
            Portas = new List<string>();
            dgvPortas.DataSource = null;
            dgvPortas.Rows.Clear();
            DataTable table = new DataTable();
            object[] array = new object[1];

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2",
              "SELECT * FROM Win32_PnPEntity WHERE ConfigManagerErrorCode = 0");

            foreach (ManagementObject obj in searcher.Get())
            {
                object captionObj = obj["Caption"];
                if (captionObj != null)
                {
                    string caption = captionObj.ToString();
                    if (caption.Contains("Wireless Application (COM"))
                    {
                        Portas.Add(caption);
                        dgvPortas.Rows.Add(caption);
                    }
                }
            }
        }
        private void EnviarSMS()
        {
           
            int retorno = sms.EnviarSms(Numero, Mensagem);
            if (retorno > -1)
                MessageBox.Show("SMS Enviado");
            else
                MessageBox.Show("SMS falhou");
        }
        private void btnTestar_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Portas.Count; i++)
            {
                string numero = "+55" + txtTelefone.Text;
                string mensagem = "Porta:" + Portas[i];
                Numero = numero;
                Mensagem = mensagem;
                EnviarSMS();
            }
        }

        private void btnSelecionar_Click(object sender, EventArgs e)
        {
            string p = dgvPortas.CurrentRow.Cells[0].Value.ToString();
            try
            {
                int posi = p.IndexOf("(");
                int posf = p.IndexOf(")");
                int tamanho = posf - posi;
                if(!Principal.COMs.Contains(p.Substring(posi + 1, tamanho - 1)))
                Principal.COMs.Add(p.Substring(posi + 1, tamanho - 1));
            }
            catch (Exception)
            {
               
            }
            
            this.Close();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!sp.IsOpen)
                {
                    sp.Open();
                }
                System.Threading.Thread.Sleep(2000);
              var aux = sp.ReadLine();
            }
            catch (Exception ex)
            {
            
            }
        }

        private void btnVarredura_Click(object sender, EventArgs e)
        {
            BuscarTordeira();
        }
    }
}
