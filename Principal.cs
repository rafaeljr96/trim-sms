﻿using EnviarSMS;
using Solimob.Domain.Entities;
using Solimob.Infra.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Torpedos
{
    public partial class Principal : Form
    {
        private static bool ok = true;
        private static int torpedo;
        private SmsClass sms = new SmsClass();
        private int sleep = 900;
        public Principal()
        {
            
            Console.Read();
            InitializeComponent();
            Inicia_Thread();
            dgvSMS.AutoGenerateColumns = true;
            COMs = SmsClass.BuscarTordeira();
            // dgvSMS.DataSource = new List<Torpedo>();//  new TorpedoRepository().GetAll().ToList();
        }
        public static List<string> COMs = new List<string>();
        public List<string> Linhas = new List<string>();
        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Torpedos T = new Torpedos();
            T.Show();
        }
        private void Inicia_Thread()
        {
            new Thread(new ThreadStart(Thread_dgvPortasCOM)).Start();
            new Thread(new ThreadStart(Thread_dgvSMS)).Start();
        }
        delegate void SetTextCallback();
        private void Adiciona_dgvPortasCOM()
        {
            if (COMs.Count != dgvPortasCOM.RowCount)
            {
                dgvPortasCOM.Rows.Clear();
                foreach (string str in COMs)
                    dgvPortasCOM.Rows.Add(str);
                sms.AtualizaPortas(COMs);
            }
        }
        private void Adiciona_dgvSMS()
        {
            List<Torpedo> torpedos = null;
            if (cbQtd.SelectedItem == null)
            {
                torpedos = new TorpedoRepository().GetAll(10).ToList();
                cbQtd.SelectedIndex = 0;
            }
            else
            torpedos = new TorpedoRepository().GetAll(int.Parse(cbQtd.Items[cbQtd.SelectedIndex].ToString())).ToList();
            if (torpedos == null)
                return;
            torpedos = torpedos.OrderByDescending(x => x.Id).ToList();
            var torpedosAtuais = (from row in dgvSMS.Rows.Cast<DataGridViewRow>() select new Torpedo { Id = int.Parse(row.Cells[0].Value.ToString()) }).ToList();
            var TorpedosNovos = (from c in torpedos where !(from c1 in torpedosAtuais select c1.Id).Contains(c.Id) select c).ToList();
            foreach (Torpedo c in TorpedosNovos)
                dgvSMS.Rows.Add(c.Id, c.Destino, c.Mensagem, c.DataCadastro, c.Enviado, c.Entrega,c.Origem,c.Fiscalizacao_Status!=null? c.Fiscalizacao_Status.Descricao:"");// = lista;
            if (SmsClass.serialport != null && SmsClass.serialport.Length > 0)
            {
                torpedosAtuais = (from row in dgvSMS.Rows.Cast<DataGridViewRow>()
                                  where (row.Cells[4].Value.ToString() == "False")
                                  select
                                new Torpedo
                                {
                                    Id = int.Parse(row.Cells[0].Value.ToString()),
                                }).ToList();
                foreach (var item in torpedosAtuais)
                {
                    torpedo = item.Id;
                    if( EnviarSMS()>-1)
                    foreach (var linha in dgvSMS.Rows.Cast<DataGridViewRow>())
                    {
                        if (linha.Cells[0].Value.ToString() == item.Id + "")
                            linha.Cells[4].Value = "True";
                    }
                }
            }

        }
        private int EnviarSMS()
        {
            Torpedo torp = new TorpedoRepository().GetById(torpedo);
            int retorno = new SmsClass().EnviarSms(torp.Destino, torp.Mensagem);  
            if(retorno>=0)
            {
                new TorpedoRepository().Update(new Torpedo()
                {
                    Id = torp.Id,
                    DataCadastro = torp.DataCadastro,
                    Destino = torp.Destino,
                    Entrega = torp.Entrega,
                    Enviado = true,
                    Fiscalizacao_StatusId = torp.Fiscalizacao_StatusId,
                    Mensagem = torp.Mensagem,
                    Origem = torp.Origem
                });                
            }
            return retorno;
        }
        private void Thread_dgvPortasCOM()
        {
            while (ok)
            {
                Thread.Sleep(sleep);
                if (dgvPortasCOM.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(Adiciona_dgvPortasCOM);
                    try
                    {
                        Invoke(d);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }
        private void Thread_dgvSMS()
        {
            while (ok)
            {
                Thread.Sleep(sleep*2);
                if (dgvSMS.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(Adiciona_dgvSMS);
                    try
                    {
                        Invoke(d);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }
      
        private void btnPare_Click(object sender, EventArgs e)
        {
            btnIniciar.Dispose();
        }
        private void btnAdicionaCOM_Click(object sender, EventArgs e)
        {
            BuscaTorpedeira BT = new BuscaTorpedeira();
            BT.Show();
        }
       
        private void Principal_Load(object sender, EventArgs e)
        {
            Timer1.Start();
            cbQtd.Items.Clear();
            cbQtd.Items.Add("10");
            cbQtd.Items.Add("20");
            cbQtd.Items.Add("100");
        }
        private void btnIniciar_Click(object sender, EventArgs e)
        {
            //AtualizaSMSs();
        }
        private void Principal_FormClosed(object sender, FormClosedEventArgs e)
        {
            ok = false;
        }
        private void btnExcluiCOM_Click(object sender, EventArgs e)
        {
            string select = dgvPortasCOM.SelectedCells[0].Value.ToString();
            Principal.COMs.Remove(select);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            dgvSMS.Rows.Clear();
        }
        private void Timer1_Tick(object sender, EventArgs e)//para enviar SMS 1 vez ao dia
        {
            DateTime HorarioAtual = DateTime.Now;

            if ((HorarioAtual.Hour == 9) && (HorarioAtual.Minute == 05) && (HorarioAtual.Second == 00))
            {
                for ( int i=0; SmsClass.serialport!=null &&  i <SmsClass.serialport.Length;i++)
                {
                    string Mensagem = "Bom dia !!! Mensagem SMS Trim port: " + SmsClass.serialport[i].PortName;
                    new SmsClass().EnviarSms("18996106757", Mensagem);
                    new SmsClass().EnviarSms("18997260225", Mensagem);
                }
            }            
        }
    }
}
