﻿namespace Torpedos
{
    partial class BuscaTorpedeira
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvPortas = new System.Windows.Forms.DataGridView();
            this.PortaCOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnVarredura = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTestar = new System.Windows.Forms.Button();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.btnSelecionar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPortas)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPortas
            // 
            this.dgvPortas.AllowUserToAddRows = false;
            this.dgvPortas.AllowUserToDeleteRows = false;
            this.dgvPortas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPortas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PortaCOM});
            this.dgvPortas.Location = new System.Drawing.Point(10, 11);
            this.dgvPortas.Name = "dgvPortas";
            this.dgvPortas.ReadOnly = true;
            this.dgvPortas.Size = new System.Drawing.Size(257, 184);
            this.dgvPortas.TabIndex = 1;
            this.dgvPortas.DoubleClick += new System.EventHandler(this.btnSelecionar_Click);
            // 
            // PortaCOM
            // 
            this.PortaCOM.FillWeight = 300F;
            this.PortaCOM.HeaderText = "Porta COM";
            this.PortaCOM.Name = "PortaCOM";
            this.PortaCOM.ReadOnly = true;
            this.PortaCOM.Width = 300;
            // 
            // btnVarredura
            // 
            this.btnVarredura.Location = new System.Drawing.Point(13, 201);
            this.btnVarredura.Name = "btnVarredura";
            this.btnVarredura.Size = new System.Drawing.Size(75, 23);
            this.btnVarredura.TabIndex = 13;
            this.btnVarredura.Text = "Atualizar";
            this.btnVarredura.Click += new System.EventHandler(this.btnVarredura_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 249);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Nº Celular";
            // 
            // btnTestar
            // 
            this.btnTestar.Location = new System.Drawing.Point(192, 263);
            this.btnTestar.Name = "btnTestar";
            this.btnTestar.Size = new System.Drawing.Size(75, 23);
            this.btnTestar.TabIndex = 9;
            this.btnTestar.Text = "Testar";
            this.btnTestar.UseVisualStyleBackColor = true;
            this.btnTestar.Click += new System.EventHandler(this.btnTestar_Click);
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(10, 265);
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(100, 20);
            this.txtTelefone.TabIndex = 14;
            // 
            // btnSelecionar
            // 
            this.btnSelecionar.Location = new System.Drawing.Point(192, 201);
            this.btnSelecionar.Name = "btnSelecionar";
            this.btnSelecionar.Size = new System.Drawing.Size(75, 23);
            this.btnSelecionar.TabIndex = 12;
            this.btnSelecionar.Text = "Selecionar";
            this.btnSelecionar.UseVisualStyleBackColor = true;
            this.btnSelecionar.Click += new System.EventHandler(this.btnSelecionar_Click);
            // 
            // BuscaTorpedeira
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(276, 298);
            this.Controls.Add(this.btnSelecionar);
            this.Controls.Add(this.btnVarredura);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnTestar);
            this.Controls.Add(this.txtTelefone);
            this.Controls.Add(this.dgvPortas);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "BuscaTorpedeira";
            this.Text = "BuscaTorpedeiracs";
            this.Load += new System.EventHandler(this.BuscaTorpedeira_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPortas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPortas;
        private System.Windows.Forms.DataGridViewTextBoxColumn PortaCOM;
        private System.Windows.Forms.Button btnVarredura;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnTestar;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.Button btnSelecionar;
    }
}