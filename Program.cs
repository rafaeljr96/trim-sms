﻿using System;
using System.Windows.Forms;

namespace Torpedos
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Principal P = new Principal();
            P.Text = "SMS TRIM";
            Application.Run(P);
        }
    }
}
