﻿namespace Torpedos
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.arquivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnIniciar = new System.Windows.Forms.Button();
            this.btnPare = new System.Windows.Forms.Button();
            this.dgvPortasCOM = new System.Windows.Forms.DataGridView();
            this.NomePorta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAdicionaCOM = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExcluiCOM = new System.Windows.Forms.Button();
            this.dgvSMS = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Destino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mensagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataCadastro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Enviado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Entrega = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Origem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fiscalizacao_StatusId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.cbQtd = new System.Windows.Forms.ComboBox();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPortasCOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSMS)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arquivoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(658, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // arquivoToolStripMenuItem
            // 
            this.arquivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sairToolStripMenuItem});
            this.arquivoToolStripMenuItem.Name = "arquivoToolStripMenuItem";
            this.arquivoToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.arquivoToolStripMenuItem.Text = "Arquivo";
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(93, 22);
            this.sairToolStripMenuItem.Text = "Sair";
            // 
            // btnIniciar
            // 
            this.btnIniciar.Location = new System.Drawing.Point(14, 385);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(75, 23);
            this.btnIniciar.TabIndex = 11;
            this.btnIniciar.Text = "Iniciar";
            this.btnIniciar.UseVisualStyleBackColor = true;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // btnPare
            // 
            this.btnPare.Location = new System.Drawing.Point(95, 385);
            this.btnPare.Name = "btnPare";
            this.btnPare.Size = new System.Drawing.Size(75, 23);
            this.btnPare.TabIndex = 12;
            this.btnPare.Text = "Parar";
            this.btnPare.UseVisualStyleBackColor = true;
            this.btnPare.Click += new System.EventHandler(this.btnPare_Click);
            // 
            // dgvPortasCOM
            // 
            this.dgvPortasCOM.AllowUserToAddRows = false;
            this.dgvPortasCOM.AllowUserToDeleteRows = false;
            this.dgvPortasCOM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPortasCOM.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NomePorta});
            this.dgvPortasCOM.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgvPortasCOM.Location = new System.Drawing.Point(14, 49);
            this.dgvPortasCOM.MultiSelect = false;
            this.dgvPortasCOM.Name = "dgvPortasCOM";
            this.dgvPortasCOM.ReadOnly = true;
            this.dgvPortasCOM.RowHeadersVisible = false;
            this.dgvPortasCOM.Size = new System.Drawing.Size(83, 71);
            this.dgvPortasCOM.TabIndex = 13;
            // 
            // NomePorta
            // 
            this.NomePorta.FillWeight = 80F;
            this.NomePorta.HeaderText = "Porta";
            this.NomePorta.Name = "NomePorta";
            this.NomePorta.ReadOnly = true;
            this.NomePorta.Width = 80;
            // 
            // btnAdicionaCOM
            // 
            this.btnAdicionaCOM.Location = new System.Drawing.Point(122, 68);
            this.btnAdicionaCOM.Name = "btnAdicionaCOM";
            this.btnAdicionaCOM.Size = new System.Drawing.Size(75, 23);
            this.btnAdicionaCOM.TabIndex = 14;
            this.btnAdicionaCOM.Text = "Adicionar";
            this.btnAdicionaCOM.UseVisualStyleBackColor = true;
            this.btnAdicionaCOM.Click += new System.EventHandler(this.btnAdicionaCOM_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Torpedeiras Encontradas";
            // 
            // btnExcluiCOM
            // 
            this.btnExcluiCOM.Location = new System.Drawing.Point(122, 97);
            this.btnExcluiCOM.Name = "btnExcluiCOM";
            this.btnExcluiCOM.Size = new System.Drawing.Size(75, 23);
            this.btnExcluiCOM.TabIndex = 16;
            this.btnExcluiCOM.Text = "Excluir";
            this.btnExcluiCOM.UseVisualStyleBackColor = true;
            this.btnExcluiCOM.Click += new System.EventHandler(this.btnExcluiCOM_Click);
            // 
            // dgvSMS
            // 
            this.dgvSMS.AllowUserToAddRows = false;
            this.dgvSMS.AllowUserToDeleteRows = false;
            this.dgvSMS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSMS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvSMS.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvSMS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSMS.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Destino,
            this.Mensagem,
            this.DataCadastro,
            this.Enviado,
            this.Entrega,
            this.Origem,
            this.Fiscalizacao_StatusId});
            this.dgvSMS.Location = new System.Drawing.Point(12, 162);
            this.dgvSMS.MultiSelect = false;
            this.dgvSMS.Name = "dgvSMS";
            this.dgvSMS.ReadOnly = true;
            this.dgvSMS.RowHeadersVisible = false;
            this.dgvSMS.Size = new System.Drawing.Size(635, 217);
            this.dgvSMS.TabIndex = 18;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.FillWeight = 50F;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 41;
            // 
            // Destino
            // 
            this.Destino.DataPropertyName = "Destino";
            this.Destino.HeaderText = "Destino";
            this.Destino.Name = "Destino";
            this.Destino.ReadOnly = true;
            this.Destino.Width = 68;
            // 
            // Mensagem
            // 
            this.Mensagem.DataPropertyName = "Mensagem";
            this.Mensagem.HeaderText = "Mensagem";
            this.Mensagem.Name = "Mensagem";
            this.Mensagem.ReadOnly = true;
            this.Mensagem.Width = 84;
            // 
            // DataCadastro
            // 
            this.DataCadastro.DataPropertyName = "DataCadastro";
            this.DataCadastro.HeaderText = "DataCadastro";
            this.DataCadastro.Name = "DataCadastro";
            this.DataCadastro.ReadOnly = true;
            this.DataCadastro.Width = 97;
            // 
            // Enviado
            // 
            this.Enviado.DataPropertyName = "Enviado";
            this.Enviado.HeaderText = "Enviado";
            this.Enviado.Name = "Enviado";
            this.Enviado.ReadOnly = true;
            this.Enviado.Width = 71;
            // 
            // Entrega
            // 
            this.Entrega.DataPropertyName = "Entrega";
            this.Entrega.HeaderText = "Entrega";
            this.Entrega.Name = "Entrega";
            this.Entrega.ReadOnly = true;
            this.Entrega.Width = 69;
            // 
            // Origem
            // 
            this.Origem.HeaderText = "Origem";
            this.Origem.Name = "Origem";
            this.Origem.ReadOnly = true;
            this.Origem.Width = 65;
            // 
            // Fiscalizacao_StatusId
            // 
            this.Fiscalizacao_StatusId.DataPropertyName = "Fiscalizacao_Status";
            this.Fiscalizacao_StatusId.HeaderText = "Fiscalizacao_StatusId";
            this.Fiscalizacao_StatusId.Name = "Fiscalizacao_StatusId";
            this.Fiscalizacao_StatusId.ReadOnly = true;
            this.Fiscalizacao_StatusId.Width = 135;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 142);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "SMSs";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(50, 138);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 19);
            this.button1.TabIndex = 20;
            this.button1.Text = "Atualizar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbQtd
            // 
            this.cbQtd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbQtd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbQtd.FormattingEnabled = true;
            this.cbQtd.Items.AddRange(new object[] {
            "10",
            "50",
            "100",
            "200"});
            this.cbQtd.Location = new System.Drawing.Point(582, 134);
            this.cbQtd.Name = "cbQtd";
            this.cbQtd.Size = new System.Drawing.Size(64, 21);
            this.cbQtd.TabIndex = 21;
            // 
            // Timer1
            // 
            this.Timer1.Enabled = true;
            this.Timer1.Interval = 1000;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 420);
            this.Controls.Add(this.cbQtd);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvSMS);
            this.Controls.Add(this.btnExcluiCOM);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAdicionaCOM);
            this.Controls.Add(this.dgvPortasCOM);
            this.Controls.Add(this.btnPare);
            this.Controls.Add(this.btnIniciar);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Principal";
            this.Text = "Principal";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Principal_FormClosed);
            this.Load += new System.EventHandler(this.Principal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPortasCOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSMS)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem arquivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.Button btnPare;
        private System.Windows.Forms.DataGridView dgvPortasCOM;
        private System.Windows.Forms.Button btnAdicionaCOM;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnExcluiCOM;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Destino;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mensagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataCadastro;
        private System.Windows.Forms.DataGridViewTextBoxColumn Enviado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Entrega;
        private System.Windows.Forms.DataGridViewTextBoxColumn Origem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fiscalizacao_StatusId;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomePorta;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbQtd;
        private System.Windows.Forms.DataGridView dgvSMS;
        private System.Windows.Forms.Timer Timer1;
    }
}