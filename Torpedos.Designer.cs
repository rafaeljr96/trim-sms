﻿namespace Torpedos
{
    partial class Torpedos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvPortas = new System.Windows.Forms.DataGridView();
            this.NomePorta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txTelefone = new System.Windows.Forms.MaskedTextBox();
            this.btnTestar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txNumeroEnvio = new System.Windows.Forms.TextBox();
            this.lbNumero = new System.Windows.Forms.Label();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnVarredura = new System.Windows.Forms.Button();
            this.btnApagar = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.txPorta = new System.Windows.Forms.TextBox();
            this.txModelo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPortas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPortas
            // 
            this.dgvPortas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPortas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NomePorta});
            this.dgvPortas.Location = new System.Drawing.Point(11, 274);
            this.dgvPortas.Name = "dgvPortas";
            this.dgvPortas.Size = new System.Drawing.Size(402, 184);
            this.dgvPortas.TabIndex = 0;
            this.dgvPortas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPortas_CellContentClick);
            this.dgvPortas.DoubleClick += new System.EventHandler(this.duploClik);
            // 
            // NomePorta
            // 
            this.NomePorta.FillWeight = 300F;
            this.NomePorta.HeaderText = "Nome Porta";
            this.NomePorta.Name = "NomePorta";
            this.NomePorta.Width = 300;
            // 
            // txTelefone
            // 
            this.txTelefone.Location = new System.Drawing.Point(156, 489);
            this.txTelefone.Name = "txTelefone";
            this.txTelefone.Size = new System.Drawing.Size(176, 20);
            this.txTelefone.TabIndex = 1;
            // 
            // btnTestar
            // 
            this.btnTestar.Location = new System.Drawing.Point(338, 486);
            this.btnTestar.Name = "btnTestar";
            this.btnTestar.Size = new System.Drawing.Size(75, 23);
            this.btnTestar.TabIndex = 2;
            this.btnTestar.Text = "Testar";
            this.btnTestar.UseVisualStyleBackColor = true;
            this.btnTestar.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(153, 473);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Telefone:";
            // 
            // txNumeroEnvio
            // 
            this.txNumeroEnvio.Location = new System.Drawing.Point(11, 221);
            this.txNumeroEnvio.Name = "txNumeroEnvio";
            this.txNumeroEnvio.Size = new System.Drawing.Size(161, 20);
            this.txNumeroEnvio.TabIndex = 4;
            // 
            // lbNumero
            // 
            this.lbNumero.AutoSize = true;
            this.lbNumero.Location = new System.Drawing.Point(12, 205);
            this.lbNumero.Name = "lbNumero";
            this.lbNumero.Size = new System.Drawing.Size(76, 13);
            this.lbNumero.TabIndex = 5;
            this.lbNumero.Text = "Numero envio:";
            // 
            // btnGravar
            // 
            this.btnGravar.Location = new System.Drawing.Point(697, 218);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 6;
            this.btnGravar.Text = "Gravar";
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnVarredura
            // 
            this.btnVarredura.Location = new System.Drawing.Point(11, 464);
            this.btnVarredura.Name = "btnVarredura";
            this.btnVarredura.Size = new System.Drawing.Size(75, 23);
            this.btnVarredura.TabIndex = 7;
            this.btnVarredura.Text = "Varredura";
            this.btnVarredura.UseVisualStyleBackColor = true;
            this.btnVarredura.Click += new System.EventHandler(this.btnVarredura_Click);
            // 
            // btnApagar
            // 
            this.btnApagar.Location = new System.Drawing.Point(641, 12);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(131, 23);
            this.btnApagar.TabIndex = 8;
            this.btnApagar.Text = "Deletar Selecionadas";
            this.btnApagar.UseVisualStyleBackColor = true;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 43);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(760, 150);
            this.dataGridView1.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Cadastradas";
            // 
            // txPorta
            // 
            this.txPorta.Location = new System.Drawing.Point(381, 221);
            this.txPorta.Name = "txPorta";
            this.txPorta.ReadOnly = true;
            this.txPorta.Size = new System.Drawing.Size(186, 20);
            this.txPorta.TabIndex = 11;
            this.txPorta.TextChanged += new System.EventHandler(this.txPorta_TextChanged);
            // 
            // txModelo
            // 
            this.txModelo.Location = new System.Drawing.Point(188, 221);
            this.txModelo.Name = "txModelo";
            this.txModelo.Size = new System.Drawing.Size(178, 20);
            this.txModelo.TabIndex = 12;
            this.txModelo.TextChanged += new System.EventHandler(this.txModelo_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(378, 205);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Porta:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(185, 205);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Modelo:";
            // 
            // Torpedos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txModelo);
            this.Controls.Add(this.txPorta);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnApagar);
            this.Controls.Add(this.btnVarredura);
            this.Controls.Add(this.btnGravar);
            this.Controls.Add(this.lbNumero);
            this.Controls.Add(this.txNumeroEnvio);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnTestar);
            this.Controls.Add(this.txTelefone);
            this.Controls.Add(this.dgvPortas);
            this.Name = "Torpedos";
            this.Text = "CadastroTorpedeira";
            this.Load += new System.EventHandler(this.Torpedos_Load);
            this.TextChanged += new System.EventHandler(this.btnApagar_Click);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPortas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPortas;
        private System.Windows.Forms.MaskedTextBox txTelefone;
        private System.Windows.Forms.Button btnTestar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txNumeroEnvio;
        private System.Windows.Forms.Label lbNumero;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomePorta;
        private System.Windows.Forms.Button btnVarredura;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txPorta;
        private System.Windows.Forms.TextBox txModelo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

