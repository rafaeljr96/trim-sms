﻿using Solimob.Domain.Entities;
using Solimob.Infra.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Management;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace EnviarSMS
{

    public delegate void FuncaoCallback(int resultado);

    public class SmsClass
    {
        public static string recebe = "";
        public static string recebeSms = "";
        public static string telefone = "";
        public static string numeroLinha = "";
        public static string modelo = "";
        public static string dataString = "";
        public static SerialPort[] serialport { set; get; }
        public SerialPort sp { set; get; }
        public int pos { set; get; } = 0;
        private int porta { set; get; } = 0;//variavel que devini a porta que sera enviada o SMS
        public SmsClass() { }

        private int Atual()//funcao para retornar a possição da porta que sera enviada
        {
            if (serialport == null)
                return -1;
            if (porta + 1 >= serialport.Length)
                return porta = 0;
            return porta + 1;
        }

        public int EnviarSms(string telefone, string mensagem)
        {
            mensagem = mensagem.ToUpper();
            mensagem = mensagem.Replace("Á", "A").Replace("Â", "A").Replace("À", "A").Replace("Å", "A").Replace("Ã", "A").Replace("Ä", "A");
            mensagem = mensagem.Replace("É", "E").Replace("Ê", "E").Replace("È", "E").Replace("Ë", "E");
            mensagem = mensagem.Replace("Í ", "I").Replace("Î", "I").Replace("Ì", "I").Replace("Ï", "I");
            mensagem = mensagem.Replace("Ó", "O").Replace("Ô", "O").Replace("Ò", "O").Replace("Õ", "O").Replace("Ö", "O");
            mensagem = mensagem.Replace("Ú", "U").Replace("Û", "U").Replace("Ù", "U").Replace("Ü", "U");
            mensagem = mensagem.Replace("Ç", "C");
            mensagem = mensagem.Replace("Ñ", "N");

            if (telefone[0] == '0')
                telefone = "+55" + telefone.Remove(0, 1);
            int pos = Atual();
            int qtd = 0;
            while (!serialport[pos].IsOpen && qtd < 100)//até encontrar uma porta averta
            {
                pos = Atual();
                qtd++;
                Thread.Sleep(50);
            }//5 Segundos
            if (qtd == 100)
                return -1;
            int sleep = 1000;
            try
            {
                //serialport[pos].Open();
                serialport[pos].WriteLine("AT" + (char)(13)); // escreve na porta serie "AT + char(13)"
                Thread.Sleep(sleep); //delay                                       
                serialport[pos].WriteLine("AT+CMGF=1" + (char)(13));
                Thread.Sleep(sleep);
                serialport[pos].WriteLine("AT+CMGR=2" + (char)(13));
                Thread.Sleep(sleep);
                serialport[pos].WriteLine("AT+CSMP=49,167,0,0" + (char)(13));//confirmação SMS
                Thread.Sleep(sleep);
                serialport[pos].WriteLine("AT+CNMI=2,1,0,1,0" + (char)(13));//confirmação SMS
                Thread.Sleep(sleep);
                //serialport[pos].WriteLine("AT+CNMI=1,2,0,0,0" + (char)(13));//
                serialport[pos].WriteLine("AT+CSMS=1" + (char)(13)); //escreve na porta serie "AT+CMGF=1 + char(13)"
                Thread.Sleep(sleep); // delay
                serialport[pos].WriteLine("AT+CMGS=\"" + telefone + "\"");
                Thread.Sleep(sleep); //delay
                serialport[pos].WriteLine("" + mensagem + (char)(26));
                Thread.Sleep(sleep);
                serialport[pos].WriteLine("AT+CMGL=REC UNREAD");
                Thread.Sleep(sleep);
                Thread.Sleep(sleep);
                serialport[pos].WriteLine("AT+CMT");
                Thread.Sleep(sleep * 2);
                recebe = serialport[pos].ReadExisting();
                //serialport[pos].Close();
            }
            catch (Exception ex)
            {
                return -1;
            }
            return pos;
        }

        void _Serial_DataReceived(object sender, SerialDataReceivedEventArgs e)//receber SMS
        {
            try
            {
                SerialPort porta = (SerialPort)sender;
                recebe = porta.ReadExisting();
                porta.DiscardInBuffer();
                if (recebe.Contains("+CDS:"))//confirma rec SMS
                {
                    telefone = recebe.Substring(18, 11);
                    Torpedo torp = new TorpedoRepository().GetNumero(telefone);
                    if (torp != null)
                        new TorpedoRepository().Update(new Torpedo()
                        {
                            Id = torp.Id,
                            DataCadastro = torp.DataCadastro,
                            Destino = torp.Destino,
                            Entrega = recebe,
                            Enviado = true,
                            Fiscalizacao_StatusId = torp.Fiscalizacao_StatusId,
                            Mensagem = torp.Mensagem,
                            Origem = torp.Origem
                        }
                                );
                }
                Console.Write(recebe);
            }
            catch (Exception ex)
            {
                //  MessageBox.Show(recebe);
            }
        }
        public int busca(string[] vrecebe)
        {
            int pos = 0;
            bool flag = false;
            for (int i = 0; i < vrecebe.Length && !flag; i++)
            {
                if (vrecebe[i].Contains("CMT") || vrecebe[i].Contains("CMGR") && !vrecebe[i].Contains("CMTI"))
                {
                    pos = i;
                    flag = true;
                }
            }
            return pos;
        }

        public void AtualizaPortas(List<string> coms)
        {
            try
            {
                serialport = new SerialPort[coms.Count];
                int i = 0;
                foreach (string p in coms)
                {
                    int posi = p.IndexOf("(");
                    int posf = p.IndexOf(")");
                    int tamanho = posf - posi;
                    string auxp = "";
                    serialport[i] = new SerialPort();
                    if (p.Length > 6)
                    {
                        auxp = p.Substring(posi + 1, tamanho - 1);
                        serialport[i].PortName = auxp;
                    }
                    else
                        serialport[i].PortName = p;

                    serialport[i].Encoding = Encoding.ASCII;
                    serialport[i].BaudRate = 9600;
                    serialport[i].Parity = Parity.None; // define a paridade como nenhuma
                    serialport[i].DataBits = 8; //define os bits de dados como 8 bits
                    serialport[i].StopBits = StopBits.One; // define o valor de stopbits como 1
                    serialport[i].Handshake = Handshake.RequestToSend; // define a forma com é feito o HandShake
                    serialport[i].DtrEnable = true; //activa o DTR
                    serialport[i].RtsEnable = true; //activa o RTS
                    serialport[i].NewLine = System.Environment.NewLine;
                    serialport[i].ReadTimeout = 300;
                    serialport[i].WriteTimeout = 300;
                    serialport[i].DataReceived += new SerialDataReceivedEventHandler(_Serial_DataReceived); //This is to add event handler delegate when data is received by the port
                    serialport[i].Open();
                    Handshake h = serialport[i].Handshake;
                    i++;
                }
            }
            catch (Exception ex)
            {
                Console.Write("Erro ao conectar na porta");
             
            }
            
        }
        public static List<string> BuscarTordeira()
        {
            List<string> Portas = new List<string>();
            object[] array = new object[1];
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2",
              "SELECT * FROM Win32_PnPEntity WHERE ConfigManagerErrorCode = 0");
            foreach (ManagementObject obj in searcher.Get())
            {
                object captionObj = obj["Caption"];
                if (captionObj != null)
                {
                    string caption = captionObj.ToString();
                    if (caption.Contains("Wireless Application (COM"))
                    {
                        int posi = caption.IndexOf("(");
                        int posf = caption.IndexOf(")");
                        int tamanho = posf - posi;
                        if (!Portas.Contains(caption.Substring(posi + 1, tamanho - 1)))
                            Portas.Add(caption.Substring(posi + 1, tamanho - 1));
                    }
                }
            }
            return Portas;
        }

    }


}
