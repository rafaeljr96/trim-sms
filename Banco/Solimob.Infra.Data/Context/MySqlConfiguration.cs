﻿using MySql.Data.Entity;
using System.Data.Entity;

namespace Solimob.Infra.Data.Context
{
    public class MySqlConfiguration : DbConfiguration
    {
        public MySqlConfiguration()
        {
            SetHistoryContext("MySql.Data.MySqlClient", (conn, schema) => new MySqlHistoryContext(conn, schema));
        }
    }
}
