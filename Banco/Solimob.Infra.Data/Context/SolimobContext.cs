﻿using Solimob.Domain.Entities;
using Solimob.Infra.Data.EntityMapping;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;

namespace Solimob.Infra.Data.Context
{
    public class SolimobContext : DbContext
    {
        //public SolimobContext() : base("Local")
        public SolimobContext() : base("Contexto")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties()
                .Where(p => p.Name == p.ReflectedType.Name + "Id")
                .Configure(p => p.IsKey());

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            // modelBuilder.Properties<string>()
            //    .Configure(p => p.HasMaxLength(100));

            modelBuilder.Configurations.Add(new AvaliacaoMapping());
            modelBuilder.Configurations.Add(new CadastroMunicipalMapping());
            modelBuilder.Configurations.Add(new CidadeMapping());
            modelBuilder.Configurations.Add(new ClienteMapping());
            modelBuilder.Configurations.Add(new ComodoMapping());
            modelBuilder.Configurations.Add(new CondicoesMapping());
            modelBuilder.Configurations.Add(new CondominioMapping());
            modelBuilder.Configurations.Add(new CorretorMapping());
            modelBuilder.Configurations.Add(new EnderecoMapping());
            modelBuilder.Configurations.Add(new EsquadriaMapping());
            modelBuilder.Configurations.Add(new EstadoCivilMapping());
            modelBuilder.Configurations.Add(new EstadoMapping());
            modelBuilder.Configurations.Add(new Fiscalizacao_StatusMapping());
            modelBuilder.Configurations.Add(new FiscalizacaoMapping());
            modelBuilder.Configurations.Add(new FiscalizacaoTipoStatusMapping());
            modelBuilder.Configurations.Add(new ImagemMapping());
            modelBuilder.Configurations.Add(new ImobiliariaMapping());
            modelBuilder.Configurations.Add(new ImovelAreadeLazerMapping());
            modelBuilder.Configurations.Add(new ImovelLazerMapping());
            modelBuilder.Configurations.Add(new ImovelArmarioMapping());
            modelBuilder.Configurations.Add(new ImovelPisoMapping());
            modelBuilder.Configurations.Add(new ImovelServicoMapping());
            modelBuilder.Configurations.Add(new ImovelSocialMapping());
            modelBuilder.Configurations.Add(new ImovelFachadaMapping());
            modelBuilder.Configurations.Add(new ImovelCaracteristicasMapping());
            modelBuilder.Configurations.Add(new ImovelCaractertisticasComercialMapping());
            modelBuilder.Configurations.Add(new ImovelMapping());
            modelBuilder.Configurations.Add(new NewComentarioMapping());
            modelBuilder.Configurations.Add(new NewMapping());
            modelBuilder.Configurations.Add(new PessoaFisicaMapping());
            modelBuilder.Configurations.Add(new PessoaJuridicaMapping());
            modelBuilder.Configurations.Add(new PessoaMapping());
            modelBuilder.Configurations.Add(new NewPatrocinada_PessoaMapping());
            modelBuilder.Configurations.Add(new PessoaRegIdMapping());
            modelBuilder.Configurations.Add(new TipoComodoMapping());
            modelBuilder.Configurations.Add(new PessoasFiscalizadasMapping());
            modelBuilder.Configurations.Add(new TipoDivulgacaoMapping());
            modelBuilder.Configurations.Add(new TipoFiscalizacaoMapping());
            modelBuilder.Configurations.Add(new TipoImovelMapping());
            modelBuilder.Configurations.Add(new TorpedosMapping());
            modelBuilder.Configurations.Add(new VisitaMapping());

            modelBuilder.Entity<EstadoCivil>().Property(m => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None); //desabilitar autoincremento
            modelBuilder.Entity<TipoImovel>().Property(m => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None); //desabilitar autoincremento
            modelBuilder.Entity<TipoComodo>().Property(m => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None); //desabilitar autoincremento
            modelBuilder.Entity<ImovelFachada>().Property(m => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None); //desabilitar autoincremento
            modelBuilder.Entity<ImovelSolar>().Property(m => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None); //desabilitar autoincremento
            modelBuilder.Entity<FiscalizacaoTipoStatus>().Property(m => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None); //desabilitar autoincremento
            //modelBuilder.Entity<Pessoa>().Property(m => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None); //desabilitar autoincremento

        }
        public static DateTime HrBrasilia()
        {
            DateTime dateTime = DateTime.UtcNow;
            TimeZoneInfo hrBrasilia = TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, hrBrasilia);
        }
        public override int SaveChanges()
        {
            try
            {
                foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("DataCadastro") != null))
                {
                    if (entry.State == EntityState.Added)
                    {
                        entry.Property("DataCadastro").CurrentValue = HrBrasilia();
                    }

                    if (entry.State == EntityState.Modified)
                    {
                        entry.Property("DataCadastro").IsModified = false;
                    }
                }
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
#if DEBUG
                List<string> Erros = new List<string>();
                foreach (var eve in ex.EntityValidationErrors)
                {
                    string erro = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Erros.Add(erro);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        erro = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        Erros.Add(erro);
                    }
                }

#endif
                throw new Exception(ex.Message);
                return -1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                return -1;
            }

        }

        public DbSet<Avaliacao> Avaliacao { get; set; }
        public DbSet<CadastroMunicipal> CadastroMunicipal { get; set; }
        public DbSet<Cidade> Cidade { get; set; }
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Comodo> Comodo { get; set; }
        public DbSet<Condicoes> Condicoes { get; set; }
        public DbSet<Corretor> Corretor { get; set; }
        public DbSet<Endereco> Endereco { get; set; }
        public DbSet<Esquadria> Esquadria { get; set; }
        public DbSet<Estado> Estado { get; set; }
        public DbSet<EstadoCivil> EstadoCivil { get; set; }
        public DbSet<Fiscalizacao_Status> Fiscalizacao_Status { get; set; }
        public DbSet<Fiscalizacao> Fiscalizacao { get; set; }
        public DbSet<NewPatrocinada_Pessoa> NewPatrocinada_Pessoa { get; set; }
        public DbSet<FiscalizacaoTipoStatus> FiscalizacaoStatus { get; set; }
        public DbSet<Imagem> Imagem { get; set; }
        public DbSet<Imobiliaria> Imobiliaria { get; set; }
        public DbSet<Imovel> Imovel { get; set; }
        public DbSet<ImovelSocial> ImovelSocial { get; set; }
        public DbSet<ImovelServico> ImovelServico { get; set; }
        public DbSet<ImovelPiso> ImovelPiso { get; set; }
        public DbSet<ImovelLazer> ImovelLazer { get; set; }
        public DbSet<ImovelArmario> ImovelArmario { get; set; }
        public DbSet<ImovelSolar> ImovelSolar { get; set; }
        public DbSet<ImovelFachada> ImovelFachada { get; set; }
        public DbSet<ImovelCaracteristicas> ImovelCaracteristicas { get; set; }
        public DbSet<ImovelCaractertisticasComercial> ImovelCaractertisticasComercial { get; set; }
        public DbSet<ImovelAreadeLazer> ImovelEstrutura { get; set; }
        public DbSet<NewComentario> NewComentario { get; set; }
        public DbSet<New> New { get; set; }
        public DbSet<Pessoa> Pessoa { get; set; }
        public DbSet<PessoaFisica> PessoaFisica { get; set; }
        public DbSet<PessoaJuridica> PessoaJuridica { get; set; }
        public DbSet<PessoaRegId> PessoaRegId { get; set; }
        public DbSet<PessoasFiscalizadas> PessoasFiscalizada { get; set; }
        public DbSet<TipoComodo> TipoComodo { get; set; }
        public DbSet<TipoDivulgacao> TipoDivulgacao { get; set; }
        public DbSet<TipoFiscalizacao> TipoFiscalizacao { get; set; }
        public DbSet<TipoImovel> TipoImovel { get; set; }
        public DbSet<Torpedo> Torpedo { get; set; }

    }
}
