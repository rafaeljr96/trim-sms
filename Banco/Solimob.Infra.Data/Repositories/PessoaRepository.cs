﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System.Linq;
using System.Data.Entity;

namespace Solimob.Infra.Data.Repositories
{
    public class PessoaRepository : RepositoryBase<Pessoa>, IPessoaRepository
    {
        public int Status(int id)
        {
            Pessoa p = Db.Pessoa.Find(id);
            p.Ativo = !p.Ativo;
            return Db.SaveChanges();
        }
        public new int Update(Pessoa obj)
        {
            Pessoa p = Db.Pessoa.Find(obj.Id);
            p.Token = obj.Token;
            return Db.SaveChanges();
        }
        public Pessoa GetByCreci(string creci)
        {
            creci = creci.ToUpper().Replace("F", "").Replace("J", "");
            Pessoa pessoa = Db.Pessoa.Where(x => x.Creci==creci + "F").Include(x => x.Endereco).FirstOrDefault();
            if (pessoa == null)
                pessoa = Db.Pessoa.Where(x => x.Creci==creci + "J").Include(x => x.Endereco).FirstOrDefault();
            if(pessoa!=null)
                pessoa.Senha = "";
            return pessoa;
        }
        public Pessoa GetByCreciSenha(string creci)
        {
            creci = creci.ToUpper().Replace("F", "").Replace("J", "");
            Pessoa pessoa = Db.Pessoa.Where(x => x.Creci == creci + "F").Include(x => x.Endereco).FirstOrDefault();
            if (pessoa == null)
                pessoa = Db.Pessoa.Where(x => x.Creci == creci + "J").Include(x => x.Endereco).FirstOrDefault();           
            return pessoa;
        }
        public new Pessoa GetById(int id)
        {
            return Db.Pessoa.Where(x => x.Id == id).Include(x => x.PessoaRegId).FirstOrDefault();
        }

    }
}
