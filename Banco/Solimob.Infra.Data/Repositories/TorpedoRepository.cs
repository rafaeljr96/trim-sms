﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class TorpedoRepository : RepositoryBase<Torpedo>, ITorpedoRepository
    {
        public new int Add(Torpedo obj)
        {
            if (obj.Destino != null)
            {
                obj.Destino = obj.Destino.Replace("(", "").Replace(")", "").Replace("-", "");
                if ((obj.Destino.Length == 11 || obj.Destino.Length == 12) && (obj.Destino[3] == '9' || obj.Destino[3] == '8'))
                {
                    Db.Set<Torpedo>().Add(obj);
                    return Db.SaveChanges();
                }
            }
            return -1;
        }
        public List<Torpedo> GetAll(int qtd)
        {
            return Db.Torpedo.OrderBy(x=>x.Enviado).ThenBy(x=>x.Id).Take(qtd).ToList();

        }
        public Torpedo GetNumero(string numero)
        {
            return Db.Torpedo.Where(x => x.Destino == numero && x.Enviado).OrderBy(x => x.DataCadastro).FirstOrDefault();
        }
    }
}
