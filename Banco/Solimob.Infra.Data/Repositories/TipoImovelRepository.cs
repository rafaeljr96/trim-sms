﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class TipoImovelRepository : RepositoryBase<TipoImovel>, ITipoImovelRepository
    {
        public IEnumerable<TipoImovel> GetFilho(int id)
        {
            if(id==0)
                return Db.TipoImovel.Where(x=>x.TipoImovelId==null).ToList();
            return Db.TipoImovel.Where(x=>x.TipoImovelId==id).ToList();
        }
    }
}
