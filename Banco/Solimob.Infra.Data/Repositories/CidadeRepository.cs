﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class CidadeRepository : RepositoryBase<Cidade>, ICidadeRepository
    {
        public ICollection<Cidade>  GetByUF(string EstadoSigla)
        {
            ICollection<Cidade> Cidades = Db.Cidade.Where(c => c.EstadoSigla == EstadoSigla).ToList();

            return Cidades;
        }

    }
}
