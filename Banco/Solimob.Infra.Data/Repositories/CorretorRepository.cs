﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System;
using System.Data.Entity;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class CorretorRepository : RepositoryBase<Corretor>, ICorretorRepository
    {
        public new int Add(Corretor obj)
        {
            obj.PessoaFisica.Pessoa.Tipo = 4;
            Db.Set<Corretor>().Add(obj);
            return Db.SaveChanges();
        }

        public Corretor GetByCreci(string creci)
        {
            return Db.Corretor.Where(x => x.PessoaFisica.Pessoa.Creci == creci).FirstOrDefault();
        }

        public new int Update(Corretor obj)
        {
            try
            {
                Db.Entry(obj).State = EntityState.Modified;
                Db.Entry(obj.PessoaFisica).State = EntityState.Modified;
                Db.Entry(obj.PessoaFisica.Pessoa).State = EntityState.Modified;
                Db.Entry(obj.PessoaFisica.Pessoa.Endereco).State = EntityState.Modified;
                return Db.SaveChanges();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                throw new Exception();
            }
        }
    }
}
