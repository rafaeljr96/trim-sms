﻿using Solimob.Domain.Interfaces.Repositories;
using Solimob.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected SolimobContext Db = new SolimobContext();

        public int Add(TEntity obj)
        {
            Db.Set<TEntity>().Add(obj);
            return Db.SaveChanges();
        }

        public TEntity GetById(int id)
        {
            return Db.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Db.Set<TEntity>().ToList();
        }
        public int Update(TEntity obj)
        {
            try
            {
                Db.Entry(obj).State = EntityState.Modified;
                return Db.SaveChanges();
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int Remove(int id)
        {
            try
            {
                var a = GetById(id);
                Db.Set<TEntity>().Remove(a);
                return Db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
#if DEBUG
                List<string> Erros = new List<string>();
                foreach (var eve in ex.EntityValidationErrors)
                {
                    string erro = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Erros.Add(erro);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        erro = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        Erros.Add(erro);
                    }
                }
                return -1002;
#endif
                return -1002;
            }

        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
