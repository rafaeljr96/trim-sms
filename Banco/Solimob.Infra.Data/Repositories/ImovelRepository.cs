﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class ImovelRepository : RepositoryBase<Imovel>, IImovelRepository
    {
        public new int Update(Imovel obj)
        {
            try
            {
                Db.Entry(obj).State = EntityState.Modified;
                return Db.SaveChanges();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                throw;
            }
        }
        public new int Add(Imovel obj)
        {
            Db.Set<Imovel>().Add(obj);
            return Db.SaveChanges();
        }
        public new Imovel GetById(int id)
        {
            return Db.Imovel.Where(x => x.Id == id)
                 //.Include(x => x.Condominio)
                 //.Include(x => x.Endereco)
                 //.Include(x => x.CadastroMunicipal)
                 //.Include(x => x.TipoDivulgacao)
                 //.Include(x => x.TipoImovel)
                 //.Include(x => x.IPTU)
                 //.Include(x => x.Comodos)
                 //.Include(x => x.ImovelCaracteristicas)
                 //.Include(x => x.ImovelArea)
                 //.Include(x => x.Aquecimento)
                 //.Include(x => x.ImovelEstrutura)
                .Include(x => x.Imagens)
                .Include(x => x.Avaliacoes)
                .FirstOrDefault();
          
        }
        public  ICollection<Imovel> GetAll(int PessoaId)
        {
            return Db.Imovel
                .Include(x => x.ImovelLazer)
                .Include(x => x.Endereco)
                .Include(x => x.ImovelPiso)
                .Include(x => x.ImovelServico)
                .Include(x => x.TipoImovel)
                .Include(x => x.ImovelSocial)
                .Include(x => x.ImovelSolar)
                .Include(x => x.ImovelCaracteristicas)
                .Include(x => x.Imagens)
                .Include(x => x.Avaliacoes).Where(x=>x.PessoaId==PessoaId)
                .ToList();

        }

    }
}
