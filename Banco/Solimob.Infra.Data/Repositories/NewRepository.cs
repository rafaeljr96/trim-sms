﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class NewRepository : RepositoryBase<New>, INewRepository
    {

        public ICollection<New> GetAll(int PessoaId)
        {
            var ab =
             Db.New.Include(x => x.NewPessoa).Include(x => x.Imagens).Include(x => x.Pessoa.Pai).Include("Pessoa.Pai.Filhos.Filhos")
               .Where(x => x.PessoaId == PessoaId
                || x.PessoaId == 1
                || x.NewPatrocinada_Pessoa.Any(b=>b.PessoaId==PessoaId)
                || (!x.Publica && x.Pessoa.Filhos.Count>0 && x.Pessoa.Filhos.Any( y=> y.Id==PessoaId))
               || (x.Publica && x.Pessoa.PaiId !=null &&  x.Pessoa.Pai.Filhos.Any(z=>z.Id==PessoaId ))


              ).ToList();
            return ab;
   //         return Db.New.Include(x => x.NewPessoa).Include(x => x.Imagens).Include(x => x.Pessoa.Filhos).Include(x => x.Pessoa.Pai).Include(x => x.Pessoa.Pai.Filhos)
   //             .Where(x => x.PessoaId == PessoaId 
   //             || x.PessoaId==1
   ////             || x.NewPatrocinada_Pessoa.Any(b=>b.PessoaId==PessoaId) 
   //  //           || (!x.Publica && x.Pessoa.Filhos.Count>0 && x.Pessoa.Filhos.Any( y=> y.Id==PessoaId)) 
   ////            || (x.Publica && x.Pessoa.PaiId !=null &&  x.Pessoa.Pai.Filhos.Any(z=>z.Id==PessoaId || z.Filhos.Any(a=>a.Id==PessoaId)))
   //             ).ToList();
        }
        public new New GetById(int id)
        {
            return Db.New.Include(x=>x.Imagens).Where(x=> x.Id==id).FirstOrDefault();
        }
        public new int Add(New news )
        {
            if(Db.Corretor.Find(news.PessoaId)!=null)
            return -1007;
            news.QtdComentario = 0;
            news.QtdCurtidas = 0;
            Db.New.Add(news);
            return Db.SaveChanges();
        }
        public new int Update(New obj)
        {
            var banco =  GetById(obj.Id);
            banco.Conteudo = obj.Conteudo;
            banco.Publica = obj.Publica;

            if (obj.Imagens == null)
                obj.Imagens = new List<Imagem>();
            //Consulta Linq para obter as coleções que devem ser excluídas do item atual
            var ImagensDeletadas = (from c in banco.Imagens
                                    where !(from c1 in obj.Imagens
                                            select c1.Id).Contains(c.Id)
                                    select c).ToList();

            //Consulta Linq para obter as coleções que devem ser adicionadas ao item atual
            var ImagensAdicionadas = (from c in obj.Imagens
                                      where !(from c1 in banco.Imagens
                                              select c1.Id).Contains(c.Id)
                                      select c).ToList();

            //Exclusão das coleções relacionados ao item atual
            ImagensDeletadas.ForEach(c => banco.Imagens.Remove(c));
            ImagensDeletadas.ForEach(c => Db.Imagem.Remove(c));

            //Adição das novas coleções relacionadas ao item atual
            foreach (Imagem c in ImagensAdicionadas)
            {
                //Como os objetos Colecao vieram de outro contexto é necessário verificar se os mesmos já foram "Attachados" a esse
                if (Db.Entry(c).State == System.Data.Entity.EntityState.Detached && c.Id != 0)
                    Db.Imagem.Attach(c);
                banco.Imagens.Add(c);
            }
            return Db.SaveChanges();
        }
        public ICollection<Pessoa> Curtidas(int id)
        {
            New noticia = Db.New.Include(x => x.NewPessoa).Where(x => x.Id == id).FirstOrDefault();
            return noticia.NewPessoa;
        }
        public ICollection<NewComentario> Comentarios(int id)
        {
            New noticia = Db.New.Include(x => x.NewComentario).Where(x => x.Id == id).FirstOrDefault();
            return noticia.NewComentario.OrderBy(x=>x.DataCadastro).ToList();
        }
        public int addCurtida(int NewId,int PessoaId)
        {
            New noticia = Db.New.Include(x => x.NewPessoa).Where(x=>x.Id== NewId).FirstOrDefault();
            Pessoa pessoa = Db.Pessoa.Include(x => x.New).Where(x => x.Id == PessoaId).FirstOrDefault();
            if (noticia.NewPessoa.Any(x=> x.Id==pessoa.Id))//ja curtiu
            {
                noticia.NewPessoa.Remove(pessoa);
                pessoa.New.Remove(noticia);
                noticia.QtdCurtidas--;
            }
            else
            {
                noticia.NewPessoa.Add(pessoa);
                pessoa.New.Add(noticia);
                noticia.QtdCurtidas++;
            }
            return Db.SaveChanges();
        }
        public int addComentario(NewComentario comentario)
        {
            
            Db.Set<NewComentario>().Add(comentario);
            New noticia = Db.New.Find(comentario.NewId);
            if (noticia!=null)
            {
                noticia.QtdComentario++;
                return Db.SaveChanges();
            }
            return -1;
        }

    }
}
