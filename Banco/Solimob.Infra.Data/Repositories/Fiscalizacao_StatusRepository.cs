﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class Fiscalizacao_StatusRepository : RepositoryBase<Fiscalizacao_Status>, IFiscalizacao_StatusRepository
    {
        public ICollection<Fiscalizacao_Status> GetAll(int FiscalizacaoId)
        {
            return Db.Set<Fiscalizacao_Status>().Where(x => x.FiscalizacaoId == FiscalizacaoId).ToList();
        }
        public new int Add(Fiscalizacao_Status obj)
        {
            try
            {
                Fiscalizacao fiscalizacao = Db.Fiscalizacao.Find(obj.FiscalizacaoId);
                fiscalizacao.FiscalizacaoTipoStatusId = obj.FiscalizacaoTipoStatusId;
                //if (fiscalizacao.DenunciadoId != null && fiscalizacao.DenunciadoId != 0)
                //{
                //    Imobiliaria aux = Db.Imobiliaria.Find(fiscalizacao.DenunciadoId);
                //    string mensagem = "ADICIONADO UM NOVO STATUS NO FISCALIZAÇÃO " + fiscalizacao.Endereco.Logradouro + " " + obj.Descricao;
                //    mensagem = mensagem.Length > 180 ? mensagem.Substring(0, 177) + "..." : mensagem;
                //    if (aux.PessoaJuridica.Pessoa.Telefone1.Length>8)
                //    Db.Torpedo.Add(new Torpedo()
                //    {
                //        Destino = aux.PessoaJuridica.Pessoa.Telefone1,
                //        Enviado = false,
                //        Mensagem = mensagem.Length > 180 ? mensagem.Substring(0, 177) + "..." : mensagem,
                //});
                //}
                Db.Fiscalizacao_Status.Add(obj);

                if (obj.FiscalizacaoTipoStatusId ==3 || obj.FiscalizacaoTipoStatusId==5)//finalizado
                    fiscalizacao.Finalizado = true;

                return Db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
#if DEBUG
                List<string> Erros = new List<string>();
                foreach (var eve in ex.EntityValidationErrors)
                {
                    string erro = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Erros.Add(erro);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        erro = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        Erros.Add(erro);
                    }
                }
                return -1;
#endif
            }
        }
    }
    
}
