﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;

namespace Solimob.Infra.Data.Repositories
{
    public class TipoComodoRepository : RepositoryBase<TipoComodo>, ITipoComodoRepository
    {
    }
}
