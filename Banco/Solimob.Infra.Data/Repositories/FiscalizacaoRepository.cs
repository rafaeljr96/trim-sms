﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class FiscalizacaoRepository : RepositoryBase<Fiscalizacao>, IFiscalizacaoRepository
    {
        public int Visualizada(int id)
        {
            Fiscalizacao fis = Db.Fiscalizacao.Find(id);
            fis.Visualizado = true;
            return Db.SaveChanges();
        }
        public new Fiscalizacao GetById(int id)
        {
            return Db.Fiscalizacao.Where(x => x.Id == id).Include("Imagens").Include(x => x.FiscalizacaoTipoStatus).Include("PessoasFiscalizadas").Include("Fiscalizacao_Status").Include("Solicitante").Include("TipoFiscalizacao").Include("Endereco").FirstOrDefault();
        }
        public ICollection<Fiscalizacao> GetAll(int idPessoa)
        {
            Imobiliaria imob = Db.Imobiliaria.Find(idPessoa);
            if (imob == null)
                return Db.Set<Fiscalizacao>().Where(x => x.SolicitanteId == idPessoa || x.DenunciadoId == idPessoa).Include("Imagens").Include("Solicitante").Include("TipoFiscalizacao").Include("Endereco").OrderBy(x => x.Finalizado).ToList();
            return Db.Set<Fiscalizacao>().Where(x => x.SolicitanteId == idPessoa || x.DenunciadoId == idPessoa || x.Solicitante.PessoaFisica.Corretor.PessoaFisica.Pessoa.PaiId == idPessoa || x.DenunciadoId == idPessoa).Include("Imagens").Include("Solicitante").Include("TipoFiscalizacao").Include("Endereco").OrderBy(x => x.Finalizado).ToList();
        }
        public new int Update(Fiscalizacao obj)
        {
            try
            {
                Fiscalizacao banco = GetById(obj.Id);
                if (banco.SolicitanteId != obj.SolicitanteId)
                    return -1;
                banco.Descricao = obj.Descricao;
                banco.PlantaoCoordenador = obj.PlantaoCoordenador;
                banco.PlantaoNome = obj.PlantaoNome;
                banco.PFCreci = obj.PFCreci;
                banco.PFNome = obj.PFNome;
                banco.PFApelido = obj.PFApelido;
                banco.PFTelefone = obj.PFTelefone;
                banco.PFGerente = obj.PFGerente;
                banco.PFApelidoGerente = obj.PFApelidoGerente;
                banco.web = obj.web;
                //banco.WebApelido = obj.WebApelido;
                //banco.WebCreci = obj.WebCreci;
                //banco.WebNome = obj.WebNome;
                //banco.WebTelefone = obj.WebTelefone;
                banco.WebLink = obj.WebLink;
                banco.Endereco.Logradouro = obj.Endereco.Logradouro;
                banco.Endereco.Numero = obj.Endereco.Numero;
                banco.Endereco.Referencia = obj.Endereco.Referencia;
                banco.Endereco.Complemento = obj.Endereco.Complemento;
                banco.Endereco.CidadeId = obj.Endereco.CidadeId;
                banco.Endereco.CEP = obj.Endereco.CEP;
                banco.Endereco.Bairro = obj.Endereco.Bairro;
                banco.DenunciadoId = obj.DenunciadoId;

                // if (obj.ImobiliariaPessoaId == 0)
                //    Db.Set<Imobiliaria>().Add(obj.Imobiliaria);

                while (banco.PessoasFiscalizadas.Count > 0)
                    Db.Set<PessoasFiscalizadas>().Remove(banco.PessoasFiscalizadas[0]);
                if (obj.PessoasFiscalizadas != null)
                    foreach (var item in obj.PessoasFiscalizadas)
                        banco.PessoasFiscalizadas.Add(item);

                if (obj.Imagens == null)
                    obj.Imagens = new List<Imagem>();
                //Consulta Linq para obter as coleções que devem ser excluídas do item atual
                var ImagensDeletadas = (from c in banco.Imagens
                                        where !(from c1 in obj.Imagens
                                                select c1.Id).Contains(c.Id)
                                        select c).ToList();

                //Consulta Linq para obter as coleções que devem ser adicionadas ao item atual
                var ImagensAdicionadas = (from c in obj.Imagens
                                          where !(from c1 in banco.Imagens
                                                  select c1.Id).Contains(c.Id)
                                          select c).ToList();

                //Exclusão das coleções relacionados ao item atual
                ImagensDeletadas.ForEach(c => banco.Imagens.Remove(c));
                ImagensDeletadas.ForEach(c => Db.Imagem.Remove(c));

                //Adição das novas coleções relacionadas ao item atual
                foreach (Imagem c in ImagensAdicionadas)
                {
                    //Como os objetos Colecao vieram de outro contexto é necessário verificar se os mesmos já foram "Attachados" a esse
                    if (Db.Entry(c).State == System.Data.Entity.EntityState.Detached && c.Id != 0)
                        Db.Imagem.Attach(c);
                    banco.Imagens.Add(c);
                }

                //Confirmação dos dados no BD
                return Db.SaveChanges();
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        public new int Add(Fiscalizacao obj)
        {
            try
            {
                obj.Fiscalizacao_Status.Add(new Fiscalizacao_Status()
                {
                    PessoaId = obj.SolicitanteId,
                    Descricao = "ABERTA VIA SISTEMA",
                    FiscalizacaoTipoStatusId = 1,
                });
                obj.FiscalizacaoTipoStatusId = 1;//Aguardando 1º Contato                
                Db.Fiscalizacao.Add(obj);
                Db.Database.CommandTimeout = 10;
                return Db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
#if DEBUG
                List<string> Erros = new List<string>();
                foreach (var eve in ex.EntityValidationErrors)
                {
                    string erro = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Erros.Add(erro);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        erro = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        Erros.Add(erro);
                    }
                }
                return -2000;
#endif
            }

        }
    }
}
