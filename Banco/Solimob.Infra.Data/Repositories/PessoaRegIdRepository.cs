﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System;

using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class PessoaRegIdRepository : RepositoryBase<PessoaRegId>, IPessoaRegIdRepository
    {
        private static DateTime HrBrasilia()
        {
            DateTime dateTime = DateTime.UtcNow;
            TimeZoneInfo hrBrasilia = TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, hrBrasilia);
        }
        public new int Add(PessoaRegId obj)
        {
            obj.Acesso = HrBrasilia();
            PessoaRegId token = Db.PessoaRegId.Where(x => x.PessoaId ==obj.PessoaId && x.RegId==obj.RegId).FirstOrDefault();
            if (token == null)
                Db.PessoaRegId.Add(obj);
            else
                token.Acesso = HrBrasilia();           
            return Db.SaveChanges();
        }
        public int Remove(string RegId, int PessoaId)
        {
            try
            {
                PessoaRegId token = Db.PessoaRegId.Where(x => x.PessoaId == PessoaId && x.RegId == RegId).FirstOrDefault();
                Db.PessoaRegId.Remove(token);
                return Db.SaveChanges();
            }
            catch (Exception e) {
                return -1;
            }
            }
    }
}
