﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class EstadoCivilRepository : RepositoryBase<EstadoCivil>, IEstadoCivilRepository
    {
        public new int Add(EstadoCivil estadoCivil)
        {
            try
            {
                if (Db.EstadoCivil.Where(x => x.Descricao == estadoCivil.Descricao).FirstOrDefault() == null)
                {
                    if (estadoCivil.Id == 0)
                    {
                        estadoCivil.Descricao = estadoCivil.Descricao.Trim();
                        Db.EstadoCivil.Add(estadoCivil);
                        return Db.SaveChanges();
                    }
                    else
                    {
                        Db.Entry(estadoCivil).State = EntityState.Modified;
                        return Db.SaveChanges();
                    }
                }
                else
                    return -1001;
            }
            catch (DbEntityValidationException ex)
            {
#if DEBUG
                List<string> Erros = new List<string>();
                foreach (var eve in ex.EntityValidationErrors)
                {
                    string erro = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Erros.Add(erro);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        erro = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        Erros.Add(erro);
                    }
                }
                return -1002;
#endif
            }
        }
    }
}
