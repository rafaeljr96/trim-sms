﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using Solimob.Infra.Data.Context;
using System;
using System.Data.Entity;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class LoginRepository : IDisposable, ILoginRepository
    {
        protected SolimobContext Db = new SolimobContext();
        public Pessoa login(string creci, string senha)
        {
            creci = creci.ToUpper().Replace("F", "").Replace("J", "");
            Pessoa pessoa = Db.Pessoa.Where(x => (x.Creci == creci + "F" || x.Creci == creci + "J" || x.Creci == creci) && x.Senha == senha).FirstOrDefault();
            return pessoa;
        }
        public void Dispose()
        {
            Db.Dispose();
        }

    }
}
