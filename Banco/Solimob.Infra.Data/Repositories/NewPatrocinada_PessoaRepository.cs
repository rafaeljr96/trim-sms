﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using Solimob.Infra.Data.Context;

namespace Solimob.Infra.Data.Repositories
{
    public class NewPatrocinada_PessoaRepository : RepositoryBase<NewPatrocinada_Pessoa>, INewPatrocinada_PessoaRepository
    {
        public new int Update(NewPatrocinada_Pessoa partrocinada)
        {
                var a = Db.Set<NewPatrocinada_Pessoa>().Find(partrocinada.PessoaId, partrocinada.NewId);
                if (a.DataVisualizada == null)
                {
                    a.DataVisualizada = SolimobContext.HrBrasilia();
                    return Db.SaveChanges();
                }
            return 0;
        }
    }
}
