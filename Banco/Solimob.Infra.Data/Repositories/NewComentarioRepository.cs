﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Data.Entity.Validation;

namespace Solimob.Infra.Data.Repositories
{
    public class NewComentarioRepository : RepositoryBase<NewComentario>, INewComentarioRepository
    {
        public new int Remove(int id)
        {
            try
            {
                var a = Db.Set<NewComentario>().Find(id);
                a.New.QtdComentario--;
                Db.Set<NewComentario>().Remove(a);
                return Db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
#if DEBUG
                List<string> Erros = new List<string>();
                foreach (var eve in ex.EntityValidationErrors)
                {
                    string erro = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Erros.Add(erro);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        erro = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        Erros.Add(erro);
                    }
                }
                return -1002;
#endif
                return -1002;
            }

        }
    }
}
