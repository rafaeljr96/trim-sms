﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class ClienteRepository : RepositoryBase<Cliente>, IClientesRepository
    {
        public new void Update(Cliente obj)
        {
            try
            {
                Db.Entry(obj).State = EntityState.Modified;
                Db.Entry(obj.PessoaFisica).State = EntityState.Modified;
                Db.Entry(obj.PessoaFisica.Pessoa).State = EntityState.Modified;
                Db.Entry(obj.PessoaFisica.Pessoa.Endereco).State = EntityState.Modified;
                Db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
#if DEBUG
                List<string> Erros = new List<string>();
                foreach (var eve in ex.EntityValidationErrors)
                {
                    string erro = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Erros.Add(erro);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        erro = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        Erros.Add(erro);
                    }
                }

                throw new Exception(ex.Message);
#endif
            }
            //catch (Exception ex)
            //{
            //    System.Diagnostics.Debug.WriteLine(ex);
            //    throw new Exception();
            //}
        }
        public new Cliente GetById(int id)
        {
            try
            {
                return Db.Cliente.Where(x => x.PessoaId == id).Include("PessoaFisica").Include("PessoaFisica.Pessoa").Include("PessoaFisica.Pessoa.Enderecos").FirstOrDefault();
            }
            catch (DbEntityValidationException ex)
            {
#if DEBUG
                List<string> Erros = new List<string>();
                foreach (var eve in ex.EntityValidationErrors)
                {
                    string erro = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Erros.Add(erro);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        erro = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        Erros.Add(erro);
                    }
                }

                throw new Exception(ex.Message);
#endif
                return null;
            }
        }
    }
}
