﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Solimob.Infra.Data.Repositories
{
    public class ImobiliariaRepository : RepositoryBase<Imobiliaria>, IImobiliariaRepository
    {
        public new int Update(Imobiliaria obj)
        {
            try
            {
                Db.Entry(obj).State = EntityState.Modified;
                Db.Entry(obj.PessoaJuridica).State = EntityState.Modified;
                Db.Entry(obj.PessoaJuridica.Pessoa).State = EntityState.Modified;
                return Db.SaveChanges();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                throw;
            }
        }
        public new int Add(Imobiliaria obj)
        {
            obj.PessoaJuridica.Pessoa.Tipo = 3;
            if(getByCNPJ(obj.PessoaJuridica.CNPJ)==null && getByCRECI(obj.PessoaJuridica.Pessoa.Creci)==null)
            {
                Db.Set<Imobiliaria>().Add(obj);
                return Db.SaveChanges();
            }
            return -1;
        }
        public Imobiliaria getByCRECI(string Creci)
        {
            try
            {
                Creci = Creci.Replace("J", "");
                return Db.Imobiliaria.Where(x => x.PessoaJuridica.Pessoa.Creci.IndexOf(Creci+"J")!=-1).FirstOrDefault();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                throw;
            }
        }
        public List<Imobiliaria> getByFiltro(string filtro)
        {
            try
            {
                return Db.Imobiliaria.Where(x => x.PessoaJuridica.Pessoa.Creci.IndexOf(filtro)!=-1 || x.PessoaJuridica.Pessoa.Nome.IndexOf(filtro) != -1 || x.PessoaJuridica.Fantasia.IndexOf(filtro) != -1).ToList();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                throw;
            }
        }
        public Imobiliaria getByCNPJ(string cnpj)
        {
            try
            {
                cnpj = cnpj.Replace("/", "");
                cnpj = cnpj.Replace(".", "");
                cnpj = cnpj.Replace("-", "");
                return Db.Imobiliaria.Where(x => x.PessoaJuridica.CNPJ == cnpj).FirstOrDefault();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                throw;
            }
        }
    }
}
