namespace Solimob.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Arquivo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 100, unicode: false),
                        file = c.Binary(),
                        IdFiscalizacao = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fiscalizacao", t => t.IdFiscalizacao)
                .Index(t => t.IdFiscalizacao);
            
            CreateTable(
                "dbo.Fiscalizacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 100, unicode: false),
                        Email = c.String(nullable: false, maxLength: 150, unicode: false),
                        Telefone = c.String(nullable: false, maxLength: 11, unicode: false),
                        Referencia = c.String(maxLength: 100, unicode: false),
                        IdEndereco = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                        CondominioNome = c.String(maxLength: 100, unicode: false),
                        Descricao = c.String(maxLength: 2000, unicode: false),
                        ImobNomeEstabelecimento = c.String(maxLength: 100, unicode: false),
                        ImobCreci = c.String(maxLength: 8, unicode: false),
                        ImobNomeFantasia = c.String(maxLength: 100, unicode: false),
                        ImobTelefone = c.String(maxLength: 11, unicode: false),
                        ImobResponsavel = c.String(maxLength: 100, unicode: false),
                        PFCreci = c.String(maxLength: 8, unicode: false),
                        PFNome = c.String(maxLength: 100, unicode: false),
                        PFApelido = c.String(maxLength: 100, unicode: false),
                        PFTelefone = c.String(maxLength: 11, unicode: false),
                        PFGerente = c.String(maxLength: 100, unicode: false),
                        PFApelidoGerente = c.String(maxLength: 100, unicode: false),
                        PlantaoNome = c.String(maxLength: 100, unicode: false),
                        PlantaoCoordenador = c.String(maxLength: 100, unicode: false),
                        WebLink = c.String(maxLength: 100, unicode: false),
                        WebCreci = c.String(maxLength: 8, unicode: false),
                        WebNome = c.String(maxLength: 100, unicode: false),
                        WebApelido = c.String(maxLength: 100, unicode: false),
                        WebTelefone = c.String(maxLength: 11, unicode: false),
                        Endereco_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Endereco", t => t.Endereco_Id)
                .Index(t => t.Endereco_Id);
            
            CreateTable(
                "dbo.Endereco",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CEP = c.String(nullable: false, maxLength: 8, unicode: false),
                        Logradouro = c.String(nullable: false, maxLength: 100, unicode: false),
                        Numero = c.String(nullable: false, maxLength: 10, unicode: false),
                        Complemento = c.String(maxLength: 200, unicode: false),
                        Bairro = c.String(nullable: false, maxLength: 50, unicode: false),
                        Referencia = c.String(maxLength: 100, unicode: false),
                        IdCidade = c.Int(nullable: false),
                        IdPessoa = c.Int(),
                        IdFiscalizacao = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cidade", t => t.IdCidade)
                .ForeignKey("dbo.Pessoa", t => t.IdPessoa)
                .Index(t => t.IdCidade)
                .Index(t => t.IdPessoa);
            
            CreateTable(
                "dbo.Cidade",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50, unicode: false),
                        Ibge = c.String(nullable: false, maxLength: 10, unicode: false),
                        IdEstado = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Estado", t => t.IdEstado)
                .Index(t => t.IdEstado);
            
            CreateTable(
                "dbo.Estado",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 50, unicode: false),
                        Sigla = c.String(nullable: false, maxLength: 2, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Pessoa",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 150, unicode: false),
                        Email1 = c.String(nullable: false, maxLength: 150, unicode: false),
                        Email2 = c.String(maxLength: 150, unicode: false),
                        Creci = c.String(nullable: false, maxLength: 10, unicode: false),
                        Site = c.String(maxLength: 150, unicode: false),
                        ECliente = c.Boolean(nullable: false),
                        Senha = c.String(nullable: false, maxLength: 12, unicode: false),
                        Ativo = c.Boolean(nullable: false),
                        DataCadastro = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PessoaFisica",
                c => new
                    {
                        IdPessoa = c.Int(nullable: false),
                        RG = c.String(nullable: false, maxLength: 15, unicode: false),
                        CPF = c.String(nullable: false, maxLength: 11, unicode: false),
                        DataNascimento = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.IdPessoa)
                .ForeignKey("dbo.Pessoa", t => t.IdPessoa)
                .Index(t => t.IdPessoa);
            
            CreateTable(
                "dbo.Corretor",
                c => new
                    {
                        IdPessoa = c.Int(nullable: false),
                        BairroAtuacao = c.String(nullable: false, maxLength: 50, unicode: false),
                        Trim = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdPessoa)
                .ForeignKey("dbo.PessoaFisica", t => t.IdPessoa)
                .Index(t => t.IdPessoa);
            
            CreateTable(
                "dbo.PessoaJuridica",
                c => new
                    {
                        IdPessoa = c.Int(nullable: false),
                        Fantasia = c.String(nullable: false, maxLength: 150, unicode: false),
                        CNPJ = c.String(nullable: false, maxLength: 14, unicode: false),
                        IE = c.String(nullable: false, maxLength: 12, unicode: false),
                        DataAbertura = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.IdPessoa)
                .ForeignKey("dbo.Pessoa", t => t.IdPessoa)
                .Index(t => t.IdPessoa);
            
            CreateTable(
                "dbo.Imobiliaria",
                c => new
                    {
                        IdPessoa = c.Int(nullable: false),
                        ResponsavelTecnico = c.String(maxLength: 100, unicode: false),
                        CreciResponsavel = c.String(nullable: false, maxLength: 10, unicode: false),
                    })
                .PrimaryKey(t => t.IdPessoa)
                .ForeignKey("dbo.PessoaJuridica", t => t.IdPessoa)
                .Index(t => t.IdPessoa);
            
            CreateTable(
                "dbo.PessoasFiscalizadas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 100, unicode: false),
                        Apelido = c.String(maxLength: 100, unicode: false),
                        Telefone = c.String(maxLength: 100, unicode: false),
                        Gerente = c.String(maxLength: 100, unicode: false),
                        ApelidoGerente = c.String(maxLength: 100, unicode: false),
                        Funcao = c.String(maxLength: 100, unicode: false),
                        Tipo = c.Int(nullable: false),
                        IdFiscalizacao = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fiscalizacao", t => t.IdFiscalizacao)
                .Index(t => t.IdFiscalizacao);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PessoasFiscalizadas", "IdFiscalizacao", "dbo.Fiscalizacao");
            DropForeignKey("dbo.Fiscalizacao", "Endereco_Id", "dbo.Endereco");
            DropForeignKey("dbo.Endereco", "IdPessoa", "dbo.Pessoa");
            DropForeignKey("dbo.PessoaJuridica", "IdPessoa", "dbo.Pessoa");
            DropForeignKey("dbo.Imobiliaria", "IdPessoa", "dbo.PessoaJuridica");
            DropForeignKey("dbo.PessoaFisica", "IdPessoa", "dbo.Pessoa");
            DropForeignKey("dbo.Corretor", "IdPessoa", "dbo.PessoaFisica");
            DropForeignKey("dbo.Endereco", "IdCidade", "dbo.Cidade");
            DropForeignKey("dbo.Cidade", "IdEstado", "dbo.Estado");
            DropForeignKey("dbo.Arquivo", "IdFiscalizacao", "dbo.Fiscalizacao");
            DropIndex("dbo.PessoasFiscalizadas", new[] { "IdFiscalizacao" });
            DropIndex("dbo.Fiscalizacao", new[] { "Endereco_Id" });
            DropIndex("dbo.Endereco", new[] { "IdPessoa" });
            DropIndex("dbo.PessoaJuridica", new[] { "IdPessoa" });
            DropIndex("dbo.Imobiliaria", new[] { "IdPessoa" });
            DropIndex("dbo.PessoaFisica", new[] { "IdPessoa" });
            DropIndex("dbo.Corretor", new[] { "IdPessoa" });
            DropIndex("dbo.Endereco", new[] { "IdCidade" });
            DropIndex("dbo.Cidade", new[] { "IdEstado" });
            DropIndex("dbo.Arquivo", new[] { "IdFiscalizacao" });
            DropTable("dbo.PessoasFiscalizadas");
            DropTable("dbo.Imobiliaria");
            DropTable("dbo.PessoaJuridica");
            DropTable("dbo.Corretor");
            DropTable("dbo.PessoaFisica");
            DropTable("dbo.Pessoa");
            DropTable("dbo.Estado");
            DropTable("dbo.Cidade");
            DropTable("dbo.Endereco");
            DropTable("dbo.Fiscalizacao");
            DropTable("dbo.Arquivo");
        }
    }
}
