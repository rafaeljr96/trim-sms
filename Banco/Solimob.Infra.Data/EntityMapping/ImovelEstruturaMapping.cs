﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ImovelEstruturaMapping : EntityTypeConfiguration<ImovelEstrutura>
    {
        public ImovelEstruturaMapping()
        {
            HasKey(e => e.ImovelId);
            HasRequired(x => x.Imovel).WithRequiredDependent(x => x.ImovelEstrutura);
        }
    }
}
