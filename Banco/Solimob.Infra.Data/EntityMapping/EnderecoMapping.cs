﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class EnderecoMapping : EntityTypeConfiguration<Endereco>
    {
        public EnderecoMapping()
        {
            HasKey(e => e.Id);

            Property(e => e.Logradouro).IsRequired().HasMaxLength(100).HasColumnType("varchar");

            Property(e => e.Numero).IsRequired().HasMaxLength(10).HasColumnType("varchar");

            Property(e => e.Complemento).HasMaxLength(200).HasColumnType("varchar");

            Property(e => e.Referencia).HasMaxLength(200).HasColumnType("varchar");

            Property(e => e.Bairro).IsRequired().HasMaxLength(50).HasColumnType("varchar");

            Property(e => e.CEP).IsRequired().HasMaxLength(8).HasColumnType("varchar");

            HasOptional(e => e.Cidade).WithMany(c => c.Enderecos).HasForeignKey(c => c.CidadeId);

        }
    }
}
