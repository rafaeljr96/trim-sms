﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ImovelServicoMapping : EntityTypeConfiguration<ImovelServico>
    {
        public ImovelServicoMapping()
        {
            HasKey(e => e.ImovelId);
            HasRequired(x => x.Imovel).WithOptional(x => x.ImovelServico);
        }
    }
}
