﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class NewMapping : EntityTypeConfiguration<New>
    {
        public NewMapping()
        {
            HasKey(e => e.Id);
            Property(e => e.Conteudo).HasMaxLength(5000);
            HasRequired(e => e.Pessoa);
            HasMany(e => e.NewPessoa).WithMany(x => x.New);
        }
    }
}
