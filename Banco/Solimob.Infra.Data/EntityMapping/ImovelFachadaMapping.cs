﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ImovelFachadaMapping : EntityTypeConfiguration<ImovelFachada>
    {
        public ImovelFachadaMapping()
        {
            HasKey(t => t.Id);
        }
    }
}
