﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class TipoComodoMapping : EntityTypeConfiguration<TipoComodo>
    {
        public TipoComodoMapping()
        {
            HasKey(t => t.Id);
            HasOptional(c => c.Comodo).WithRequired(x => x.TipoComodo).Map(x => x.MapKey("IdTipoComodo"));

        }
    }
}
