﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ImovelCaractertisticasComercialMapping : EntityTypeConfiguration<ImovelCaractertisticasComercial>
    {
        public ImovelCaractertisticasComercialMapping()
        {
            HasKey(i => i.ImovelId);
            HasRequired(x => x.Imovel).WithOptional(x => x.ImovelCaractertisticasComercial);

        }
    }
}
