﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class CondicoesMapping : EntityTypeConfiguration<Condicoes>
    {
        public CondicoesMapping()
        {
            HasKey(a => a.AvaliacaoId);
            HasRequired(x => x.Avaliacao).WithRequiredDependent(x => x.Condicoes);
        }
    }
}
