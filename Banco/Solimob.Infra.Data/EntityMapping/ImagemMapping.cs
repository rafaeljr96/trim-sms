﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ImagemMapping : EntityTypeConfiguration<Imagem>
    {
        public ImagemMapping()
        {
            HasKey(a => a.Id);
            Property(a => a.img).HasColumnType("longtext");
            HasOptional(a => a.New).WithMany(a => a.Imagens).WillCascadeOnDelete();
            HasOptional(a => a.Fiscalizacao).WithMany(a => a.Imagens).WillCascadeOnDelete();
            HasOptional(a => a.Imovel).WithMany(a=>a.Imagens).WillCascadeOnDelete();
        }
    }
}
