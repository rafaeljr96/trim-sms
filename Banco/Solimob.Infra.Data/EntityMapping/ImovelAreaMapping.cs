﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    public class ImovelAreaMapping : EntityTypeConfiguration<ImovelArea>
    {
        public ImovelAreaMapping()
        {
            HasKey(i => i.ImovelId);
            HasRequired(x => x.Imovel).WithRequiredDependent(x => x.ImovelArea);
        }
    }
}
