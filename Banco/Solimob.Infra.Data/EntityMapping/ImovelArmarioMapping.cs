﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ImovelArmarioMapping : EntityTypeConfiguration<ImovelArmario>
    {
        public ImovelArmarioMapping()
        {
            HasKey(e => e.ImovelId);
            HasRequired(x => x.Imovel).WithOptional(x => x.ImovelArmario);
        }
    }
}
