﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class AvaliacaoMapping :  EntityTypeConfiguration<Avaliacao>
    {
        public AvaliacaoMapping()
        {
            HasKey(a => a.Id);
            Property(a => a.ValorVenda).IsRequired();
        }
    }
}
