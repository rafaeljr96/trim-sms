﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class TorpedosMapping : EntityTypeConfiguration<Torpedo>
    {
        public TorpedosMapping()
        {
            HasKey(e => e.Id);
            Property(e => e.Mensagem).HasMaxLength(160);
            HasOptional(e => e.Fiscalizacao_Status);
        }
    }
}
