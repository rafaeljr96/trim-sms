﻿using System.Data.Entity.ModelConfiguration;
using Solimob.Domain.Entities;

namespace Solimob.Infra.Data.EntityMapping
{
    class ImobiliariaMapping : EntityTypeConfiguration<Imobiliaria>
    {
        public ImobiliariaMapping()
        {
            HasKey(p => p.PessoaId);

            Property(p => p.CreciResponsavel).HasMaxLength(10).HasColumnType("varchar");

            HasRequired(p => p.PessoaJuridica);

        }
    }
}
