﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class CidadeMapping : EntityTypeConfiguration<Cidade>
    {
        public CidadeMapping()
        {
            HasKey(c => c.Ibge);

            Property(c => c.Nome).IsRequired().HasMaxLength(50).HasColumnType("varchar");

            Property(c => c.Ibge).IsRequired().HasMaxLength(10).HasColumnType("varchar");

            HasRequired(a => a.Estado);
        }

    }
}
