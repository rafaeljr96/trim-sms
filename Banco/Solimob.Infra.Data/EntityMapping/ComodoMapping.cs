﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ComodoMapping : EntityTypeConfiguration<Comodo>
    {
        public ComodoMapping()
        {
            HasKey(c => c.Id);
        }
    }
}
