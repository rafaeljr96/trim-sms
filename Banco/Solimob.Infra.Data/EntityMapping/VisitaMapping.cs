﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class VisitaMapping : EntityTypeConfiguration<Visita>
    {
        public VisitaMapping()
        {
            HasKey(v => v.Id);
        }
    }
}
