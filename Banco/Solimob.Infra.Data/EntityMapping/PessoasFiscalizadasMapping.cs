﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class PessoasFiscalizadasMapping : EntityTypeConfiguration<PessoasFiscalizadas>
    {
        public PessoasFiscalizadasMapping()
        {
            Property(p => p.Creci).IsOptional();
            HasOptional(x => x.Pessoa);
        }
    }
}
