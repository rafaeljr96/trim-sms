﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class EstadoMapping : EntityTypeConfiguration<Estado>
    {
        public EstadoMapping()
        {
            HasKey(e => e.Sigla);

            Property(e => e.Nome).IsRequired().HasMaxLength(50).HasColumnType("varchar");

            Property(e => e.Sigla).IsRequired().HasMaxLength(2).IsFixedLength();

        }

    }
}
