﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ClienteMapping : EntityTypeConfiguration<Cliente>
    {
        public ClienteMapping()
        {
            HasKey(p => p.PessoaId);
            HasRequired(p => p.PessoaFisica).WithOptional(x => x.Cliente);
            Property(p => p.Nacionalidade).IsRequired().HasMaxLength(50);
            Property(p => p.Profissao).IsRequired().HasMaxLength(50);
            Property(p => p.LocalTrabalho).IsRequired().HasMaxLength(50);
            Property(p => p.Cargo).IsRequired().HasMaxLength(50);
            HasMany(x => x.Visitas).WithRequired(v => v.ClienteVisitante).HasForeignKey(x => x.ClienteVisitanteId);
            HasOptional(p => p.EstadoCivil);
        }
    }
}
