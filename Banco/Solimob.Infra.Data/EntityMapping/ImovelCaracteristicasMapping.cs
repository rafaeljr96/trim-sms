﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ImovelCaracteristicasMapping : EntityTypeConfiguration<ImovelCaracteristicas>
    {
        public ImovelCaracteristicasMapping()
        {
            HasKey(i => i.ImovelId);
            HasRequired(x => x.Imovel).WithOptional(x => x.ImovelCaracteristicas);

        }
    }
}
