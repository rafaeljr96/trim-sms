﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ImovelAreadeLazerMapping : EntityTypeConfiguration<ImovelAreadeLazer>
    {
        public ImovelAreadeLazerMapping()
        {
            HasKey(e => e.ImovelId);
            HasRequired(x => x.Imovel).WithOptional(x => x.ImovelAreadeLazer);
        }
    }
}
