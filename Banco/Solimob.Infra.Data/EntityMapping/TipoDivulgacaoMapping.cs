﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class TipoDivulgacaoMapping : EntityTypeConfiguration<TipoDivulgacao>
    {
        public TipoDivulgacaoMapping()
        {
            HasKey(t => t.ImovelId);
        }
    }
}
