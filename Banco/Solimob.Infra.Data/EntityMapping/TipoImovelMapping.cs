﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class TipoImovelMapping : EntityTypeConfiguration<TipoImovel>
    {
        public TipoImovelMapping()
        {
            HasKey(t => t.Id);
            HasOptional(t => t.TipoAcima);
        }
    }
}
