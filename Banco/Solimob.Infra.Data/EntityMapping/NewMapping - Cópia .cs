﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class NewPatrocinada_PessoaMapping : EntityTypeConfiguration<NewPatrocinada_Pessoa>
    {
        public NewPatrocinada_PessoaMapping()
        {
            HasKey(e => e.NewId);
            HasRequired(e => e.Pessoa);
            HasRequired(e => e.New);
        }
    }
}
