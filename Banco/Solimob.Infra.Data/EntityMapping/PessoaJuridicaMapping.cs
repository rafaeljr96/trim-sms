﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace Solimob.Infra.Data.EntityMapping
{
    class PessoaJuridicaMapping : EntityTypeConfiguration<PessoaJuridica>
    {
        public PessoaJuridicaMapping()
        {
            HasKey(p => p.PessoaId);

            Property(p => p.Fantasia).IsRequired().HasMaxLength(150).HasColumnType("varchar");

            Property(p => p.IE).IsOptional().HasMaxLength(12).HasColumnType("varchar");

            Property(p => p.CNPJ).IsOptional().HasMaxLength(14).HasColumnType("varchar");

            Property(p => p.DataAbertura).IsOptional();

            HasRequired(p => p.Pessoa).WithOptional(p => p.PessoaJuridica);

            HasOptional(p => p.Imobiliaria);

        }
    }
}
