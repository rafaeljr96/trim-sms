﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class EsquadriaMapping : EntityTypeConfiguration<Esquadria>
    {
        public EsquadriaMapping()
        {
            HasKey(e => e.AvaliacaoId);
            HasRequired(x => x.Avaliacao).WithRequiredDependent(x => x.Esquadria);
        }
    }
}
