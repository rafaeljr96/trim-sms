﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class PessoaFisicaMapping : EntityTypeConfiguration<PessoaFisica>
    {
        public PessoaFisicaMapping()
        {
            HasKey(p => p.PessoaId);

            Property(p => p.RG).HasMaxLength(15);

            Property(p => p.CPF).IsRequired().HasMaxLength(14);

            Property(p => p.DataNascimento).IsRequired();

            HasRequired(p => p.Pessoa).WithOptional(p => p.PessoaFisica);

            HasOptional(p => p.Corretor);

        }
    }
}
