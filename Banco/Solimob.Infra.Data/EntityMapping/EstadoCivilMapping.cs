﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class EstadoCivilMapping : EntityTypeConfiguration<EstadoCivil>
    {
        public EstadoCivilMapping()
        {
            HasKey(e => e.Id);
            Property(e => e.Descricao).HasMaxLength(20);
        }
    }
}
