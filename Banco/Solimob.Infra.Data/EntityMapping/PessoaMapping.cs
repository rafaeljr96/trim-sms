﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class PessoaMapping : EntityTypeConfiguration<Pessoa>
    {
        public PessoaMapping()
        {
            HasKey(p => p.Id);

            Property(p => p.Nome).IsRequired().HasMaxLength(150);

            Property(p => p.Email1).HasMaxLength(150);

            Property(a => a.Foto).HasColumnType("longtext");

            Property(p => p.Telefone1).HasMaxLength(16);

            Property(p => p.Telefone2).HasMaxLength(16);

            Property(p => p.Creci).IsRequired().HasMaxLength(7);

            Property(p => p.Site).HasMaxLength(150);

            Property(p => p.Senha).HasMaxLength(12);


            HasOptional(e => e.Pai).WithMany(e => e.Filhos).HasForeignKey(c => c.PaiId);

            HasOptional(e => e.Endereco);
        
        }
    }
}
