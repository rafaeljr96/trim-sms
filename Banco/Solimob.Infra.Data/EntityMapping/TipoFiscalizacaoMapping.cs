﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class TipoFiscalizacaoMapping : EntityTypeConfiguration<TipoFiscalizacao>
    {
        public TipoFiscalizacaoMapping()
        {
            HasKey(e => e.Id);          
        }
    }
}
