﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    public class FiscalizacaoTipoStatusMapping : EntityTypeConfiguration<FiscalizacaoTipoStatus>
    {
        public FiscalizacaoTipoStatusMapping()
        {
            HasKey(f => f.Id);
        }
    }
}
