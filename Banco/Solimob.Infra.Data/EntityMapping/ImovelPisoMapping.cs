﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ImovelPisoMapping : EntityTypeConfiguration<ImovelPiso>
    {
        public ImovelPisoMapping()
        {
            HasKey(e => e.ImovelId);
            HasRequired(x => x.Imovel).WithOptional(x => x.ImovelPiso);
        }
    }
}
