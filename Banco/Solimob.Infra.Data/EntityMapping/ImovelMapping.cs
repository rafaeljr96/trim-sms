﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ImovelMapping : EntityTypeConfiguration<Imovel>
    {
        public ImovelMapping()
        {
            HasKey(i => i.Id);
            Property(f => f.NomeEdificio).IsOptional().HasMaxLength(100);
            HasRequired(i => i.TipoImovel); //ok
            HasRequired(i => i.Pessoa); //ok
            HasOptional(i => i.ImovelSolar); //ok
            HasOptional(i => i.ImovelFachada); //ok
            HasOptional(i => i.ImovelArmario).WithRequired(x => x.Imovel);
            HasOptional(i => i.ImovelPiso).WithRequired(x => x.Imovel);
            HasOptional(i => i.ImovelServico).WithRequired(x => x.Imovel);
            HasOptional(i => i.ImovelSocial).WithRequired(x => x.Imovel);
            HasOptional(i => i.ImovelAreadeLazer).WithRequired(x => x.Imovel);
            HasOptional(i => i.ImovelCaracteristicas).WithRequired(x => x.Imovel);
            HasOptional(i => i.ImovelLazer).WithRequired(x => x.Imovel);

            HasMany(i => i.Avaliacoes).WithRequired(x => x.Imovel).HasForeignKey(x => x.ImovelId);
            HasMany(x => x.Visitas).WithRequired(v => v.Imovel).HasForeignKey(x => x.ImovelId);
           // HasRequired(i => i.CadastroMunicipal); //ok

        }
    }
}
