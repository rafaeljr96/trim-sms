﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    public class Fiscalizacao_StatusMapping : EntityTypeConfiguration<Fiscalizacao_Status>
    {
        public Fiscalizacao_StatusMapping()
        {
            HasKey(f => f.Id);
            Property(e => e.Descricao).HasMaxLength(50).HasColumnType("varchar");
            HasRequired(f => f.Fiscalizacao).WithMany().WillCascadeOnDelete();
            HasRequired(f => f.FiscalizacaoTipoStatus).WithMany().WillCascadeOnDelete();
            HasRequired(f => f.Pessoa);
        }
    }
}
