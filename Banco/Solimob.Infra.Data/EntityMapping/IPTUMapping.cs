﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class IPTUMapping : EntityTypeConfiguration<IPTU>
    {
        public IPTUMapping()
        {
            HasKey(i => i.ImovelId);
            HasRequired(x => x.Imovel).WithRequiredDependent(x => x.IPTU);
        }
    }
}
