﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class ImovelSocialMapping : EntityTypeConfiguration<ImovelSocial>
    {
        public ImovelSocialMapping()
        {
            HasKey(e => e.ImovelId);
            HasRequired(x => x.Imovel).WithOptional(x => x.ImovelSocial);
        }
    }
}
