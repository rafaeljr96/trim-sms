﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class CorretorMapping : EntityTypeConfiguration<Corretor>
    {
        public CorretorMapping()
        {
            HasKey(c => c.PessoaId);
            Property(c => c.BairroAtuacao).HasMaxLength(50).HasColumnType("varchar");
            HasRequired(c => c.PessoaFisica);
            HasOptional(c => c.Imobiliaria);
            HasMany(c => c.Avaliacao).WithRequired(x => x.CorretorAvaliador).HasForeignKey(x=>x.CorretorAvaliadorId);

        }
    }
}
