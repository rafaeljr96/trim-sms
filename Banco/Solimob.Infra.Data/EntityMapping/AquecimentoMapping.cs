﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class AquecimentoMapping : EntityTypeConfiguration<Aquecimento>
    {
        public AquecimentoMapping()
        {
            HasKey(a => a.ImovelId);
            HasRequired(x => x.Imovel).WithRequiredDependent(x => x.Aquecimento);
        }
    }
}
