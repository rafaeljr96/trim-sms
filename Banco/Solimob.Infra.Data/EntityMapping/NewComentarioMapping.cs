﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class NewComentarioMapping : EntityTypeConfiguration<NewComentario>
    {
        public NewComentarioMapping()
        {
            HasKey(e => e.Id);
            Property(e => e.Mensagem).HasMaxLength(2000);           
            HasRequired(e => e.Pessoa);
            HasRequired(e => e.New);
        }
    }
}
