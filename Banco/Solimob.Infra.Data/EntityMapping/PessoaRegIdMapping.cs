﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class PessoaRegIdMapping : EntityTypeConfiguration<PessoaRegId>
    {
        public PessoaRegIdMapping()
        {
            HasKey(e => e.PessoaId);
            HasKey(e => e.RegId);
            Property(e => e.RegId).HasMaxLength(200);
            HasRequired(e => e.Pessoa);
        }

    }
}
