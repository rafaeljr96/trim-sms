﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    class NoticiaMapping : EntityTypeConfiguration<Noticia>
    {
        public NoticiaMapping()
        {
            HasKey(e => e.Id);
            Property(e => e.Conteudo).HasMaxLength(2000);
            Property(a => a.Imagem).HasColumnType("longtext");
            HasRequired(e => e.Pessoa);
        }
    }
}
