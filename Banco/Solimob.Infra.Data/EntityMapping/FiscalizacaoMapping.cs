﻿using Solimob.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Solimob.Infra.Data.EntityMapping
{
    public class FiscalizacaoMapping: EntityTypeConfiguration<Fiscalizacao>
    {
        public FiscalizacaoMapping()
        {
            HasKey(f => f.Id);

            Property(f => f.Descricao).IsOptional().HasMaxLength(2000);

            Property(f => f.PFCreci).IsOptional().HasMaxLength(8);

            Property(f => f.PFTelefone).IsOptional().HasMaxLength(15);

            Property(f => f.WebCreci).IsOptional().HasMaxLength(8);

            Property(f => f.WebTelefone).IsOptional().HasMaxLength(15);
            
            HasMany(f => f.PessoasFiscalizadas).WithRequired(f => f.Fiscalizacao).HasForeignKey(f => f.FiscalizacaoId).WillCascadeOnDelete();

            HasRequired(e => e.Solicitante).WithMany(e => e.FiscalizacoesSolicitadas).HasForeignKey(c => c.SolicitanteId);
            HasRequired(e => e.Denunciado).WithMany(e => e.FiscalizacoesDenunciadas).HasForeignKey(c => c.DenunciadoId);
            HasRequired(e => e.TipoFiscalizacao);
            HasRequired(e => e.Endereco);
            HasOptional(e => e.FiscalizacaoTipoStatus);
            HasOptional(e => e.Denunciado);

        }
    }
}
