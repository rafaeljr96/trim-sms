﻿using Solimob.Domain.Entities;

namespace Solimob.Domain.Interfaces.Repositories
{
    public interface IFiscalizacao_StatusRepository : IRepositoryBase<Fiscalizacao_Status>
    {
    }
}
