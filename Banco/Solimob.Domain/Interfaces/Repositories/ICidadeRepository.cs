﻿using Solimob.Domain.Entities;
using System.Collections.Generic;

namespace Solimob.Domain.Interfaces.Repositories
{
    public interface ICidadeRepository : IRepositoryBase<Cidade>
    {
        ICollection<Cidade> GetByUF(string EstadoSigla);
    }
}
