﻿using Solimob.Domain.Entities;

namespace Solimob.Domain.Interfaces.Repositories
{
    public interface ITorpedoRepository : IRepositoryBase<Torpedo>
    {
    }
}
