﻿using Solimob.Domain.Entities;

namespace Solimob.Domain.Interfaces.Repositories
{
    public interface IClientesRepository : IRepositoryBase<Cliente>
    {
    }
}
