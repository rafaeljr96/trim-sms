﻿using Solimob.Domain.Entities;

namespace Solimob.Domain.Interfaces.Repositories
{
    public interface INewPatrocinada_PessoaRepository : IRepositoryBase<NewPatrocinada_Pessoa>
    {
        
    }
}
