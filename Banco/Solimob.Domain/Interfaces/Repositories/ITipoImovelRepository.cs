﻿using Solimob.Domain.Entities;

namespace Solimob.Domain.Interfaces.Repositories
{
    public interface ITipoImovelRepository : IRepositoryBase<TipoImovel>
    {
    }
}
