﻿using Solimob.Domain.Entities;
using System.Collections.Generic;

namespace Solimob.Domain.Interfaces.Repositories
{
    public interface INoticiaRepository : IRepositoryBase<Noticia>
    {
    }
}
