﻿using Solimob.Domain.Entities;

namespace Solimob.Domain.Interfaces.Repositories
{
    public interface ILoginRepository
    {
        Pessoa login(string cpf, string senha);
        void Dispose();
    }
}
