﻿using Solimob.Domain.Entities;

namespace Solimob.Domain.Interfaces.Repositories
{
    public interface ICorretorRepository : IRepositoryBase<Corretor>
    {
        Corretor GetByCreci(string creci);
    }
}
