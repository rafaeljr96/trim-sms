﻿using System.Collections.Generic;

namespace Solimob.Domain.Interfaces.Repositories
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        int Add(TEntity obj);
        TEntity GetById(int Id);
        IEnumerable<TEntity> GetAll();
        int Update(TEntity obj);
        int Remove(int id);
        void Dispose();
    }
}
