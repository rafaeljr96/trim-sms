﻿using Solimob.Domain.Entities;

namespace Solimob.Domain.Interfaces.Services
{
    public interface IImobiliariaService : IServiceBase<Imobiliaria>
    {
    }
}
