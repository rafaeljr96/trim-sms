﻿using Solimob.Domain.Entities;

namespace Solimob.Domain.Interfaces.Services
{
    public interface ICorretorService : IServiceBase<Corretor>
    {
    }
}
