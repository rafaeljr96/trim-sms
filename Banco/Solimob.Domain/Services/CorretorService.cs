﻿using System.Collections.Generic;
using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using Solimob.Domain.Interfaces.Services;

namespace Solimob.Domain.Services
{
    public class CorretorService : ServiceBase<Corretor>, ICorretorService
    {
        private readonly ICorretorRepository _corretorRepository;

        public CorretorService(ICorretorRepository corretorRepository): base(corretorRepository)
        {
            _corretorRepository = corretorRepository;
        }
    }
}
