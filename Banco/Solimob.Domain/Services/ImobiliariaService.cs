﻿using Solimob.Domain.Entities;
using Solimob.Domain.Interfaces.Repositories;
using Solimob.Domain.Interfaces.Services;

namespace Solimob.Domain.Services
{
    class ImobiliariaService : ServiceBase<Imobiliaria>, IImobiliariaService
    {
        private readonly IImobiliariaRepository _imobiliariaRepository;

        public ImobiliariaService(IImobiliariaRepository imobiliariaRepository) : base(imobiliariaRepository)
        {
            _imobiliariaRepository = imobiliariaRepository;
        }
    }
}
