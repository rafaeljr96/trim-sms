﻿using System;
using System.Collections.Generic;

namespace Solimob.Domain.Entities
{
    public class New
    {
        public int Id { get; set; }
        public string Conteudo { get; set; }
        public virtual DateTime DataCadastro { get; set; }
        public int QtdCurtidas { get; set; }
        public int QtdComentario { get; set; }
        public bool Publica { get; set; }
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public ICollection<Imagem> Imagens { get; set; }
        public ICollection<NewComentario> NewComentario { get; set; }
        public ICollection<NewPatrocinada_Pessoa> NewPatrocinada_Pessoa { get; set; }
        public ICollection<Pessoa> NewPessoa { get; set; }
        
    }
}
