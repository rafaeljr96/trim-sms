﻿using System;

namespace Solimob.Domain.Entities
{
    public class ImovelCaractertisticasComercial
    {
        public int ImovelId { get; set; }
        public bool Agua { get; set; }
        public bool Esgoto { get; set; }
        public bool Pavimentacao { get; set; }
        public bool Energia220V { get; set; }
        public bool Energia330V { get; set; }
        public bool Bifasico { get; set; }
        public bool Trifasico { get; set; }
        public bool Doca { get; set; }
        public bool PortapCaminhao { get; set; }
        public bool EstacaodeGas { get; set; }
        public virtual Imovel Imovel { get; set; }


    }
}
