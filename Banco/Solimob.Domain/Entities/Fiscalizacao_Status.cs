﻿using System;

namespace Solimob.Domain.Entities
{
    public class Fiscalizacao_Status
    {
        public int Id { get; set; }
        public int PessoaId { get; set; }

        public int FiscalizacaoTipoStatusId { get; set; }

        public int FiscalizacaoId { get; set; }

        public virtual Pessoa Pessoa { get; set; }
        public virtual FiscalizacaoTipoStatus FiscalizacaoTipoStatus { get; set; }
        public virtual Fiscalizacao Fiscalizacao { get; set; }
        public string Descricao { get; set; }
        public DateTime DataCadastro { get; set; }

    }
}
