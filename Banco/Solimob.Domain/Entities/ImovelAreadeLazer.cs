﻿namespace Solimob.Domain.Entities
{
    public class ImovelAreadeLazer
    {
        public int ImovelId { get; set; }
        public virtual Imovel Imovel { get; set; }
        public bool Bicicletario { get; set; }
        public bool Brinquedoteca { get; set; }
        public bool CampodeFutebol { get; set; }
        public bool Churrasqueira { get; set; }
        public bool Gourmet { get; set; }
        public bool Lago { get; set; }
        public bool PianoBar { get; set; }
        public bool Piscina { get; set; }
        public bool Playground { get; set; }
        public bool QuadradeTenis { get; set; }
        public bool QuadraPoliesportiva { get; set; }
        public bool SalaFitness { get; set; }
        public bool SalaodeJogos { get; set; }
        public bool SalaodeVideoCinema { get; set; }
        public bool Sauna { get; set; }
        public bool SPA { get; set; }
        public bool Portaria24h { get; set; }
        public bool Nenhuma { get; set; }


    }
}