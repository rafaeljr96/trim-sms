﻿namespace Solimob.Domain.Entities
{
    public class ImovelCaracteristicas
    {
        public int ImovelId { get; set; }
        public bool SemiMobiliado { get; set; }
        public bool TodaMobiliada { get; set; }
        public bool ArCondicionado { get; set; }
        public bool PeDireitoDuplo { get; set; }
        public bool Deposito { get; set; }
        public bool Lareira { get; set; }
        public bool Terraco { get; set; }
        public bool JardimdeInverno { get; set; }
        public bool ElevadorProprio { get; set; }
        public virtual Imovel Imovel { get; set; }

    }
}
