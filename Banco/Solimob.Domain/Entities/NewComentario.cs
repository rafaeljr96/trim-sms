﻿using System;

namespace Solimob.Domain.Entities
{
    public class NewComentario
    {
        public int Id { get; set; }
        public string Mensagem { get; set; }
        public DateTime DataCadastro { get; set; }
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public int NewId { get; set; }
        public virtual New New { get; set; }
    }
}
