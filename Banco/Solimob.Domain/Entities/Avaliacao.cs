﻿using System;

namespace Solimob.Domain.Entities
{
    public class Avaliacao
    {
        public int Id { get; set; }
        public int ImovelId { get; set; }
        public virtual Imovel Imovel { get; set; }
        public decimal ValorVenda { get; set; }
        public DateTime DataCadastro { get; set; }
        public int CorretorAvaliadorId { get; set; }
        public virtual Corretor CorretorAvaliador { get; set; }
        public virtual Esquadria Esquadria { get; set; }
        public virtual Condicoes Condicoes { get; set; }
    }
}
