﻿namespace Solimob.Domain.Entities
{
    public class ImovelPiso
    {
        public int ImovelId { get; set; }
        public bool Porcelanato { get; set; }
        public bool Ceramico { get; set; }
        public bool Laminado { get; set; }
        public bool TacodeMadeira { get; set; }
        public bool Carpet { get; set; }
        public bool Outros { get; set; }
        public virtual Imovel Imovel { get; set; }

    }
}
