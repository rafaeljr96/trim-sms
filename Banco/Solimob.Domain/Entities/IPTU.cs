﻿namespace Solimob.Domain.Entities
{
    public class IPTU
    {
        public int ImovelId { get; set; }
        public virtual Imovel Imovel { get; set; }
        public decimal ValorAnual { get; set; }
        public decimal ValorMensal { get; set; }
        public int QuantidadeParcelas { get; set; }
        public string NumRegistro { get; set; }
    }
}
