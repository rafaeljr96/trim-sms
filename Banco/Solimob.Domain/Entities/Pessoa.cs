﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Solimob.Domain.Entities
{
    public class Pessoa
    {

        public Pessoa()
        {
            FiscalizacoesDenunciadas = new Collection<Fiscalizacao>();
            FiscalizacoesSolicitadas = new Collection<Fiscalizacao>();
            Ativo = true;
        }

        public int Id { get; set; }
        public int Tipo { get; set; } = 4;//1 ADM//2 SOLIMOB// 3 IMOBILIARIA// 4 CORRETOR
        public string Nome { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string Token { get; set; }
        public string Foto { get; set; }
        public string Telefone1 { get; set; }
        public string Telefone2 { get; set; }
        public string Creci { get; set; }
        public string Site { get; set; }
        public bool ECliente { get; set; }
        public string Senha { get; set; }
        public bool Ativo { get; set; }
        public DateTime DataCadastro { get; set; }

        public virtual PessoaFisica PessoaFisica { get; set; }
       
        public virtual PessoaJuridica PessoaJuridica { get; set; }

        public int? PaiId { get; set; }
        public virtual Pessoa Pai { get; set; }
        public int? EnderecoId { get; set; }
        public virtual Endereco Endereco { get; set; }

        public ICollection<Pessoa> Filhos { get; set; }
        public ICollection<Fiscalizacao> FiscalizacoesSolicitadas { get; set; }

        public ICollection<Fiscalizacao> FiscalizacoesDenunciadas { get; set; }

        public ICollection<PessoaRegId> PessoaRegId { get; set; }

        public ICollection<New> New { get; set; }




    }
}
