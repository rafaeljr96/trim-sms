﻿namespace Solimob.Domain.Entities
{
    public class ImovelArmario
    {
        public int ImovelId { get; set; }
        public bool NaoHa { get; set; }
        public bool Cozinha { get; set; }
        public bool Corredor { get; set; }
        public bool Closet { get; set; }
        public bool Dormitorios { get; set; }
        public bool Banheiros { get; set; }
        public bool Sala { get; set; }
        public bool Escritorio { get; set; }
        public bool Cinema_Home { get; set; }
        public bool AreadeServico { get; set; }
        public bool Dependencia { get; set; }
        public virtual Imovel Imovel { get; set; }

    }
}
