﻿namespace Solimob.Domain.Entities
{
    public class CadastroMunicipal
    {
        public int ImovelId { get; set; }
        public string Cadastro { get; set; }
        public string Quadra { get; set; }
        public string Lote { get; set; }
        public string Metragem { get; set; }
        public string Matricula { get; set; }
        public bool PrimeiroCRI{ get; set; }
        public bool SegundoCRI { get; set; }
        public string Escriturado { get; set; }
        public string Registrado { get; set; }
        public string INSS { get; set; }
        public string Habitese { get; set; }
        public string Averbado { get; set; }
        public virtual Imovel Imovel { get; set; }


    }
}
