﻿using System.Collections.Generic;

namespace Solimob.Domain.Entities
{
    public class Cidade
    {
        public string Ibge { get; set; }
        public string Nome { get; set; }
        public string EstadoSigla { get; set; }
        public virtual Estado Estado { get; set; }
        public virtual ICollection<Endereco> Enderecos{ get; set; }
    }
}
