﻿using System.Collections.Generic;

namespace Solimob.Domain.Entities
{
    public class Endereco
    {
        public int Id { get; set; }

        public string CEP { get; set; }

        public string Logradouro { get; set; }

        public string Numero { get; set; }

        public string Complemento { get; set; }

        public string Bairro { get; set; }

        public string  Referencia { get; set; }

        public string CidadeId { get; set; }

        public virtual Cidade Cidade { get; set; }
        

        
    }
}
