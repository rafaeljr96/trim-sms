﻿namespace Solimob.Domain.Entities
{
    public class ImovelEstrutura
    {
        public int ImovelId { get; set; }
        public virtual Imovel Imovel { get; set; }
        public bool Frente { get; set; }
        public bool Lateral { get; set; }
        public bool Fundos { get; set; }
        public bool Madeira { get; set; }
        public bool Alvenaria { get; set; }
        public bool Laje { get; set; }
        public bool Forro { get; set; }
    }
}