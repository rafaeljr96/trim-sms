﻿namespace Solimob.Domain.Entities
{
    public class ImovelSocial
    {
        public int ImovelId { get; set; }
        public bool Sacada { get; set; }
        public bool Escritorio { get; set; }
        public bool Varanda { get; set; }
        public bool NenhumaDelas { get; set; }
        public virtual Imovel Imovel { get; set; }

    }
}
