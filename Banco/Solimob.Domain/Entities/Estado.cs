﻿using System.Collections.Generic;

namespace Solimob.Domain.Entities
{
    public class Estado
    {
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public virtual ICollection<Cidade> Cidades{ get; set; }
    }
}
