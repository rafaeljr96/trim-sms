﻿namespace Solimob.Domain.Entities
{
    public class ImovelServico
    {
        public int ImovelId { get; set; }
        public bool Cozinha { get; set; }
        public bool Copa { get; set; }
        public bool DependenciadeFuncionarios { get; set; }
        public bool BanheiroparaFuncionarios { get; set; }
        public bool Despensa { get; set; }
        public bool AreadeServiço { get; set; }
        public bool NenhumaDelas { get; set; }
        public virtual Imovel Imovel { get; set; }

    }
}
