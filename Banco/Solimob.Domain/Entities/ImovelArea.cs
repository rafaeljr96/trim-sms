﻿namespace Solimob.Domain.Entities
{
    public class ImovelArea
    {
        public int ImovelId { get; set; }
        public virtual Imovel Imovel { get; set; }
        public double AreaTerreno { get; set; }
        public double AreaConstruida { get; set; }
        public double AreaTotal { get; set; }
    }
}