﻿namespace Solimob.Domain.Entities
{
    public class Esquadria
    {
        public int AvaliacaoId { get; set; }
        public Avaliacao Avaliacao { get; set; }
        public bool Ferro { get; set; }
        public bool Aluminio { get; set; }
        public bool Vidro { get; set; }
        public bool Madeira { get; set; }
        public bool PVC { get; set; }
    }
}
