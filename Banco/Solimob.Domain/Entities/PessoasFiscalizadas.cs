﻿namespace Solimob.Domain.Entities
{
    public class PessoasFiscalizadas
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Apelido { get; set; }
        public string Telefone { get; set; }
        public string Gerente { get; set; }
        public string ApelidoGerente { get; set; }
        public string Funcao { get; set; }
        public string Creci { get; set; }
        public int Tipo { get; set; }

        public int FiscalizacaoId { get; set; }
        public virtual Fiscalizacao Fiscalizacao { get; set; }
        public int? PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
    }
}