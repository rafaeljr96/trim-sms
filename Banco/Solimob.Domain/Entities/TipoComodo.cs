﻿using System.Collections.Generic;

namespace Solimob.Domain.Entities
{
    public class TipoComodo
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public bool Ativo { get; set; }
        public Comodo Comodo { get; set; }
    }
}
