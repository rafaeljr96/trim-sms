﻿using System.Collections.Generic;

namespace Solimob.Domain.Entities
{
    public class TipoFiscalizacao
    {
        public int Id { get; set; }
        public string Descricao { get; set; }

    }
}