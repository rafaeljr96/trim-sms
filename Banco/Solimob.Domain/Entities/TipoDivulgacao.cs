﻿namespace Solimob.Domain.Entities
{
    public class TipoDivulgacao
    {
        public int ImovelId { get; set; }
        public virtual Imovel Imovel { get; set; }
        public bool Internet { get; set; }
        public bool Jornais { get; set; }
        public bool FaixasPlacas{ get; set; }
    }
}
