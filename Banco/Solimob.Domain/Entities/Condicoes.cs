﻿namespace Solimob.Domain.Entities
{
    public class Condicoes
    {
        public int AvaliacaoId { get; set; }
        public Avaliacao Avaliacao { get; set; }
        public bool Antigo { get; set; }
        public bool Novo { get; set; }
        public bool Seminovo { get; set; }
        public bool Regular { get; set; }
        public bool Bom { get; set; }
        public bool Otimo { get; set; }
        public bool PinturaNova { get; set; }
        public int IdadeConstrucao { get; set; }
        public string AnodaConstrucao { get; set; }
    }
}