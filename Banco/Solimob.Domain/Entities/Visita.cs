﻿using System;

namespace Solimob.Domain.Entities
{
    public class Visita
    {
        public int Id { get; set; }
        public int ImovelId { get; set; }
        public Imovel Imovel { get; set; }
        public int ClienteVisitanteId { get; set; }
        public Cliente ClienteVisitante { get; set; }
        public DateTime DataVisita { get; set; }

    }
}