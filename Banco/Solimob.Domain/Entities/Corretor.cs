﻿using System.Collections.Generic;

namespace Solimob.Domain.Entities
{
    public class Corretor
    {
        public Corretor()
        {
            Trim = false;
        }

        public int PessoaId { get; set; }
        public virtual PessoaFisica PessoaFisica { get; set; }
        public string BairroAtuacao { get; set; }
        public bool Trim { get; set; }
        public bool Confrimada { get; set; }

        public int? ImobiliariaPessoaId { get; set; }
        public virtual Imobiliaria Imobiliaria { get; set; }
        public ICollection<Avaliacao> Avaliacao { get; set; }
    }
}
