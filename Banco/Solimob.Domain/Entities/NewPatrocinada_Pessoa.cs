﻿using System;

namespace Solimob.Domain.Entities
{
    public class NewPatrocinada_Pessoa
    {
        public int PessoaId { get; set; }
        public int NewId { get; set; }
        public DateTime? DataVisualizada { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public virtual New New { get; set; }
    }
}
