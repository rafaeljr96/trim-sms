﻿namespace Solimob.Domain.Entities
{
    public class ImovelLazer
    {
        public int ImovelId { get; set; }
        public bool Piscina { get; set; }
        public bool Ofuro_SPA { get; set; }
        public bool Churrasqueira { get; set; }
        public bool VarandaGourmet { get; set; }
        public bool Adega { get; set; }
        public bool Sauna { get; set; }
        public bool Solarium { get; set; }
        public bool NenhumaDelas { get; set; }
        public virtual Imovel Imovel { get; set; }

    }
}
