﻿using System;

namespace Solimob.Domain.Entities
{
    public class PessoaJuridica
    {
        public string Fantasia { get; set; }
        public string CNPJ { get; set; }
        public string IE { get; set; }
        public Boolean Grupo { get; set; }
        public DateTime? DataAbertura { get; set; }
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public virtual Imobiliaria Imobiliaria { get; set; }

    }
}
