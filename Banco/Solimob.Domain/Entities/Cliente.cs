﻿using System;
using System.Collections.Generic;

namespace Solimob.Domain.Entities
{
    public class Cliente
    {
        public int PessoaId { get; set; }
        public virtual PessoaFisica PessoaFisica { get; set; }
        public string Nacionalidade { get; set; }

        public int? EstadoCivilId { get; set; }
        public virtual EstadoCivil EstadoCivil { get; set; }
        public string Profissao { get; set; }
        public string LocalTrabalho { get; set; }
        public string Cargo { get; set; }
        public ICollection<Visita> Visitas{ get; set; }
        public DateTime DataCadastro { get; set; }

    }
}
