﻿namespace Solimob.Domain.Entities
{
    public class Imobiliaria
    {
        public int PessoaId { get; set; }
        public virtual PessoaJuridica PessoaJuridica { get; set; }
        public string ResponsavelTecnico { get; set; }
        public string CreciResponsavel { get; set; }
        public bool valida { get; set; } = false;
    }
}
