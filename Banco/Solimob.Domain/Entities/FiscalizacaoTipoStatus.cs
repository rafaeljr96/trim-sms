﻿using System;
using System.Collections.Generic;

namespace Solimob.Domain.Entities
{
    public class FiscalizacaoTipoStatus
    {
        public int Id { get; set; }
        public bool Ativo { get; set; }
        public string Nome { get; set; }

        public ICollection<Fiscalizacao_Status> Fiscalizacao_Status { get; set; }        

    }
}
