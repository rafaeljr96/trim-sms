﻿namespace Solimob.Domain.Entities
{
    public class Comodo
    {
        public int Id { get; set; }
        public int Quantidade { get; set; }
        public string Observacao { get; set; }
        public virtual TipoComodo TipoComodo { get; set; }//Quarto, Suite, Lavabo, Banheiro, Cozinha, Sala de Jantar, Sala de Visita, Jardim de Inverno, Área de Serviço, Edícula, Garagem, Segurança, Área de Lazer, Acabamentos
        public int ImovelId { get; set; }
        public virtual Imovel Imovel { get; set; }
    }
}
