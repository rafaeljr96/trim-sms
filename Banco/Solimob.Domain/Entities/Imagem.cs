﻿namespace Solimob.Domain.Entities
{
    public class Imagem
    {
        public int Id { get; set; }
        public string img { get; set; }
        public int? FiscalizacaoId { get; set; }
        public virtual Fiscalizacao Fiscalizacao { get; set; }

        public int? NewId { get; set; }
        public virtual New New { get; set; }

        public int? ImovelId { get; set; }
        public virtual Imovel Imovel { get; set; }
    }
}
