﻿using System;

namespace Solimob.Domain.Entities
{
    public class PessoaFisica
    {
        public string RG { get; set; }
        public string CPF { get; set; }
        public DateTime DataNascimento { get; set; }
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public virtual Corretor Corretor { get; set; }
        public virtual Cliente Cliente{ get; set; }
    }
}
