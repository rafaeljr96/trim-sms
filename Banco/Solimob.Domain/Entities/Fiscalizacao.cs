﻿using System;
using System.Collections.Generic;

namespace Solimob.Domain.Entities
{
    public class Fiscalizacao
    {
        public int Id { get; set; }

        public int SolicitanteId { get; set; }
        public virtual Pessoa Solicitante { get; set; }
        public int? DenunciadoId { get; set; }
        public virtual Pessoa Denunciado { get; set; }
        public string Referencia { get; set; }
        public int EnderecoId { get; set; }
        public bool web { get; set; }
        public bool Visualizado { get; set; }
        public bool Finalizado { get; set; } 
        public virtual Endereco Endereco { get; set; }
        public int TipoFiscalizacaoId { get; set; }
        public virtual TipoFiscalizacao TipoFiscalizacao { get; set; }
        public List<PessoasFiscalizadas> PessoasFiscalizadas { get; set; }
        public List<Imagem> Imagens { get; set; }
        public int? FiscalizacaoTipoStatusId { get; set; }
        public virtual FiscalizacaoTipoStatus FiscalizacaoTipoStatus { get; set; }
        public ICollection<Fiscalizacao_Status> Fiscalizacao_Status { get; set; }

        // -----------------------CONDOMÍNIO----------------------- //

        public string CondominioNome { get; set; }
        public string Descricao { get; set; }


        // -----------------------PESSOA FÍSICA----------------------- //
        public string PFCreci { get; set; }
        public string PFNome { get; set; }
        public string PFApelido { get; set; }
        public string PFTelefone { get; set; }
        public string PFGerente { get; set; }
        public string PFApelidoGerente { get; set; }

        // -----------------------PLANTÃO----------------------- //

        public string PlantaoNome { get; set; }
        public string PlantaoCoordenador { get; set; }

        // -----------------------SITES/PORTAIS/REDES SOCIAIS----------------------- //

        public string WebLink { get; set; }
        public string WebCreci { get; set; }
        public string WebNome { get; set; }
        public string WebTelefone { get; set; }
        public DateTime DataCadastro { get; set; }

    }
}
