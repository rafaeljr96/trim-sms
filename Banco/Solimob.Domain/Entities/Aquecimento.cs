﻿namespace Solimob.Domain.Entities
{
    public class Aquecimento
    {
        public int ImovelId { get; set; }
        public virtual Imovel Imovel { get; set; }
        public bool Gas { get; set; }
        public bool Eletrico { get; set; }
        public bool Solar { get; set; }
        public bool Outros { get; set; }
    }
}
