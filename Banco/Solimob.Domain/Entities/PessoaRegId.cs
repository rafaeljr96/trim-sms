﻿using System;

namespace Solimob.Domain.Entities
{
    public class PessoaRegId
    {
        public DateTime Acesso { get; set; }
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public string RegId { get; set; }
    }
}
