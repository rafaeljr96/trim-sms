﻿namespace Solimob.Domain.Entities
{
    public class TipoImovel
    {
        public int Id { get; set; }
        public int? TipoImovelId { get; set; }
        public virtual TipoImovel TipoAcima { get; set; }
        public string Descricao { get; set; }
    }
}
