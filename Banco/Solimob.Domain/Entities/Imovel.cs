﻿using System;
using System.Collections.Generic;

namespace Solimob.Domain.Entities
{
    public class Imovel
    {
        public int Id { get; set; }
        public int QtdQuartos { get; set; }
        public int QtdSalas { get; set; }
        public int QtdSuites { get; set; }
        public int QtdBanheiros { get; set; }
        public bool Lavabo { get; set; }
        public bool Trua { get; set; }
        public bool PortaoEletronico { get; set; }
        public bool Interfone { get; set; }
        public bool Mezanino { get; set; }
        public bool Locacao { get; set; }
        public decimal LocacaoValor { get; set; }
        public bool Venda { get; set; }
        public decimal VendaValor { get; set; }
        public decimal AlturaPeDireito { get; set; }

        public decimal QtdAreaUtil { get; set; }
        public decimal QtdAreaTotal { get; set; }
        public int QtdVagasGaragem { get; set; }
        public int AnoConstrucao { get; set; }
        public string Descricao { get; set; }
        public string NomeEdificio { get; set; }
        public decimal ValorCondominio { get; set; }
        public int Andar { get; set; }
        public bool Alarme { get; set; }
        public int TipoImovelId { get; set; }
        public virtual TipoImovel TipoImovel { get; set; }
        public int? ImovelSolarId { get; set; }
        public virtual ImovelSolar ImovelSolar { get; set; }
        public int? ImovelFachadaId { get; set; }
        public virtual ImovelFachada ImovelFachada { get; set; }
        public int EnderecoId { get; set; }
        public virtual Endereco Endereco { get; set; }
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public virtual ImovelArmario ImovelArmario { get; set; }
        public virtual ImovelPiso ImovelPiso { get; set; }
        public virtual ImovelSocial ImovelSocial { get; set; }
        public virtual ImovelLazer ImovelLazer { get; set; }
        public virtual ImovelCaracteristicas ImovelCaracteristicas { get; set; }
        public virtual ImovelCaractertisticasComercial ImovelCaractertisticasComercial { get; set; }
        public virtual ImovelServico ImovelServico { get; set; }
        public virtual ImovelAreadeLazer ImovelAreadeLazer { get; set; }
        public List<Imagem> Imagens { get; set; }
        public ICollection<Avaliacao> Avaliacoes { get; set; }
        public ICollection<Visita> Visitas { get; set; }
    }
}
