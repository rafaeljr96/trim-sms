﻿using System;

namespace Solimob.Domain.Entities
{
    public class Torpedo 
    {
        public int Id { get; set; }
        public string Destino { get; set; }
        public string Mensagem { get; set; }
        public DateTime DataCadastro { get; set; }
        public bool Enviado { get; set; }
        public string Entrega { get; set; }
        public string Origem { get; set; }
        public int? Fiscalizacao_StatusId { get; set; }
        public virtual Fiscalizacao_Status Fiscalizacao_Status { get; set; }
    }
}
