﻿namespace Solimob.Domain.Entities
{
    public class Condominio
    {
        public int ImovelId { get; set; }
        public virtual Imovel Imovel { get; set; }
        public string Nome { get; set; }
        public decimal MensalidadeCondominio { get; set; }
    }
}
