﻿using SMSapplication;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Management;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace EnviarSMS
{

    public class SMS
    {
        #region Private Variables
        private List<SerialPort> Portas = new List<SerialPort>();
        private SerialPort port = new SerialPort();
        private SmsHelper smsHelper = new SmsHelper();
        private ShortMessageCollection objShortMessageCollection = new ShortMessageCollection();
        #endregion
        private void AbrirConexão(string porta)
        {
            try
            {
                //Open communication port 
                this.port = smsHelper.OpenPort(porta, 9600,8, 1, 300);

                if (this.port != null)
                {
                    Portas.Add(port);
                    //this.gboPortSettings.Enabled = false;

                    ////MessageBox.Show("Modem is connected at PORT " + this.cboPortName.Text);
                    //this.statusBar1.Text = "Modem is connected at PORT " + this.cboPortName.Text;

                    ////Add tab pages
                    //this.tabSMSapplication.TabPages.Add(tbSendSMS);
                    //this.tabSMSapplication.TabPages.Add(tbReadSMS);
                    //this.tabSMSapplication.TabPages.Add(tbDeleteSMS);

                    //this.lblConnectionStatus.Text = "Connected at " + this.cboPortName.Text;
                    //this.btnDisconnect.Enabled = true;
                }
                else
                {
                    //MessageBox.Show("Invalid port settings");
                    MessageBox.Show("Invalid port settings");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void FecharConexão(string porta)
        {
            try
            {
                foreach (var item in Portas)
                {
                    if(item.PortName== porta)
                smsHelper.ClosePort(item);               
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public string[] LeMensagem(SerialPort porta)//todas 
        {
            int uCountSMS = smsHelper.CountSmsMessages(porta);
            string[] mensagens=null;
            if (uCountSMS > 0)
            {
              
                string strCommand = "AT+CMGL=\"ALL\"";

                #region Read SMS
                objShortMessageCollection = smsHelper.ReadSMS(porta, strCommand);
                mensagens = new string[objShortMessageCollection.Count];
                for (int i = 0; i < objShortMessageCollection.Count; i++)
                {
                    ShortMessage msg = objShortMessageCollection[i];
                    mensagens[i] = msg.Index + "|" + msg.Sent + "|" + msg.Sender + "|" + msg.Message;
                }                
                #endregion
            }
            return mensagens;
        }
    }

}
